package TestPackage;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import Model.Edge;
import Model.GrapheModel;
import Model.Node;


public class GraphmlExportImport {

//	LinkedList<Node> sommets;
//	LinkedList<Edge> aretes;
//	private String directory;
//	private String nomFichier;
	GrapheModel model;
	
	GraphmlExportImport(GrapheModel model_)
	{
		model=model_;
	}
		
	public void exportGraphml(String file) {
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();

			Element graphml = doc.createElement("graphml");
			graphml.setAttribute("xmln",
					"http://graphml.graphdrawing.org/xmlns");
			graphml.setAttribute("xmlns:xsi",
					"http://www.w3.org/2001/XMLSchema-instance");
			graphml.setAttribute(
					"xsi:schemaLocation",
					"http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd");

			doc.appendChild(graphml);

			Node.addAttributesGraphml(doc, graphml);
			Edge.addAttributes(doc, graphml);

			Element graph = doc.createElement("graph");
			graph.setAttribute("id", "G");
			graphml.appendChild(graph);
			// staff elements
			for (Node s : model.getNodes()) {
				graph.appendChild(s.exportNodeXml(doc));
			}
			for (Edge a : model.getEdges()) {
				graph.appendChild(a.exportEdgeXml(doc));
			}

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			
			StreamResult result = new StreamResult(new File(file));

			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(source, result);

			System.out.println("File saved!");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}

	public void importer(String file) {

		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			final DocumentBuilder builder = docFactory.newDocumentBuilder();
			final Document document = builder.parse(new File(file));
			System.out.println(file);

			final Element racine = document.getDocumentElement();
			Element personne = (Element) racine.getElementsByTagName("node")
					.item(0);
			NodeList nodes = racine.getElementsByTagName("node");
			NodeList edges = racine.getElementsByTagName("edge");

			model.getNodes().clear();
			model.getEdges().clear();

			for (int i = 0; i < nodes.getLength(); i++) {
				Element node = (Element) nodes.item(i);
				Node no = new Node(node);
				model.getNodes().add(no);
			}
			for (int i = 0; i < edges.getLength(); i++) {
				Element edge = (Element) edges.item(i);
				NodeList data = edge.getElementsByTagName("data");

				int idSource = Integer.parseInt(edge.getAttribute("source"));
				int idTarget = Integer.parseInt(edge.getAttribute("target"));

				Color col = null;
				int thick = 0;

				for (int j = 0; j < data.getLength(); j++) {
					final Element d = (Element) data.item(j);
					String key = d.getAttribute("key");
					String content = d.getTextContent();

					String name = "";
					if (key.equals("e0")) 
					{
						int red = Integer.parseInt(content.split(";")[0]);
						int green = Integer.parseInt(content.split(";")[1]);
						int blue = Integer.parseInt(content.split(";")[2]);
						col = new Color(red, green, blue);
					} else if (key.equals("e1")) {
						name = content;
					} else if (key.equals("e2")) {
						thick = Integer.parseInt(content);
					}
				}
				Edge e = new Edge(col, thick, getNodeById(idSource),
						getNodeById(idTarget));
				model.getEdges().add(e);
			}
		} catch (final ParserConfigurationException e) {
			e.printStackTrace();
		} catch (final SAXException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}

	}
	Node getNodeById(int id) {
		for (Node n : model.getNodes()) {
			if (n.getIdNode() == id) {
				return n;
			}
		}
		return null;
	}
}
