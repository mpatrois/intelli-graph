package Controller;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.LinkedList;

import Model.GraphModel;
import Model.Node;
import View.GraphView;
import View.NodeViewMovable;

public class Controller {

	private GraphModel grapheModel;
	private GraphView grapheView;

	public Controller(GraphModel model) {
		grapheModel=model;
	}
	public void setGrapheView(GraphView grapheView_)
	{
		grapheView=grapheView_;
	}
	public void coller() {
		grapheModel.past();

	}
	public void copier() {
		grapheModel.copy();
	}
	public void couper() {
		grapheModel.cut();
	}
	public void leftClick(Point point,boolean ctrl) 
	{
		if(!ctrl)
		{
			Node tmp=grapheModel.nodeClicked(point);
			/* Si clique sur aucun sommet et aucune selection en cours alors ajout d'un sommet*/
			if(tmp==null  && !grapheModel.getSelection().isElementInSelection() && !grapheModel.isInMoveMode())
			{
				grapheModel.addNode(point.x,point.y);
			}
			else if(tmp==null  && grapheModel.getSelection().isElementInSelection()){
				grapheModel.resetSelection();
			}
		}
		else
		{
			grapheModel.selectEdgeOrNode(point);
		}

	}
	public void mousePressed(Point point) 
	{
		grapheModel.setNodeTmp(grapheModel.nodeClicked(point));
	}
	public void mouseReleasedToAddEdge(Point point) 
	{
		if(!grapheModel.isInMoveMode())
		{
			Node tmp=grapheModel.nodeClicked(point);
			if(grapheModel.getNodeTmp()!=null && tmp!=null && grapheModel.getNodeTmp()!=tmp)
			{
				/* Ajout d'une arête reliant s'il en existe pas déjà une*/
				if(grapheModel.existingEdge(grapheModel.getNodeTmp(),tmp) )
				{
					grapheModel.addEdge(grapheModel.getNodeTmp(), tmp);
				}
			}
		}
	}
	public void rightClick(Point point) {
		grapheModel.nodeRightClicked(point);
	}

	public void setColor(Color c,boolean modelChange) 
	{
		if(modelChange)
			grapheModel.setDefaultColor(c);
		else
			grapheModel.setColorToSelection(c);
	}
	public void setModeDeplacement(boolean enabled) {
		grapheModel.setMoveMode(enabled);
	}
	public void setNameToSelectedElement(String text) {
		grapheModel.setNameSelectedElement(text);
	}
	public void setShapeToSelection(String selectedItem) {
		grapheModel.setTypeShapeToSelection(selectedItem);
	}
	public void setSize(int value,boolean modelChange) 
	{
		if(modelChange)
			grapheModel.setSizeDefault(value);
		else
			grapheModel.setSizeSelection(value);
	}
	public void setThickness(int value,boolean modelChange) 
	{
		if(modelChange)
			grapheModel.setThicknessDefault(value);
		else
			grapheModel.setThicknessToSelection(value);
	}

	public void setTypeShape(String selectedItem, boolean modelChange) {
		if(modelChange)
			grapheModel.setDefaultTypeShape(selectedItem);
		else
			grapheModel.setTypeShapeToSelection(selectedItem);
	}
	public void supprimerElementsSelectionnes() 
	{	
		grapheModel.deleteSelectedElements();
	}
	public void undo() {
		grapheModel.undo();
	}
	public void redo() {
		grapheModel.redo();
	}
	public void executeChromaticAlgorithm() 
	{
		grapheModel.executeChromaticAlgorithm();
	}

	public void selectNodesOrEges(Rectangle rectangle,boolean ctrl) {
		grapheModel.selectNodesOrEdges(rectangle,ctrl);
	}
	public void moveNodesToPoint(Point endLine)
	{
		grapheModel.getNodeTmp().move(endLine.x-10,endLine.y-10);
	}

	//Deplace tous les noeuds contenus dans nodesViewMovable
	//recupéré de la vue apres le mouseRelease
	public void moveNodesForReal(LinkedList<NodeViewMovable> nodesViewMovable) {
		HashMap<Node,Point> mapNodeMovable=new HashMap<>();
		for (NodeViewMovable node : nodesViewMovable) 
		{
			mapNodeMovable.put(node.getNode(), node.getPosition());
		}
		grapheModel.moveSelectedNode(mapNodeMovable);
	}
	public void executeAlgoCirc() {	grapheModel.executeCircularLayoutAlgorithm();
	}
	public void importGraphml(String fileToImport) {
		grapheModel.importGraphml(fileToImport);
	}
	public void exportGraphml(String fileToExport) {
		grapheModel.exportGraphml(fileToExport);
	}
	public void addPropertyToElement(String key, Double value) {
		grapheModel.addPropertyToElement(key,value);
	}
	public void executeAlgorithmColor(String selectedItem, Color background,Color background2) {
		grapheModel.executeAlgorithmColor(selectedItem, background, background2);
	}
	public void executeAlgoTaille(String selectedItem, int minValue, int maxValue) {
		grapheModel.executeAlgoTaille(selectedItem, minValue, maxValue);
	}
	public void executeAlgorithmDegree() {
		grapheModel.executeAlgorithmDegree();
	}
	public void exportDot(String fileToExport) {
		grapheModel.exportDot(fileToExport);
	}
	public void importDot(String fileToImport) {
		grapheModel.importDot(fileToImport);
	}
	public void removePropertyOfElement(String o) {
		grapheModel.removePropertyOfElement(o);
	}
	public void changeNameOfElement(String o) {
		grapheModel.changeNameOfElement(o);
	}

	//Execute le layout random sur la partir visible du graph
	public void executeAlgorithmRandom() {
		grapheModel.executeAlgorithmRandom(grapheView.getRealBounds());
	}


}
