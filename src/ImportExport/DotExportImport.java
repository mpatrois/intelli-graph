package ImportExport;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;

import Model.Edge;
import Model.GraphModel;
import Model.Node;

public class DotExportImport {
	GraphModel model;

	public DotExportImport(GraphModel model_)
	{
		model=model_;
	}

	public void exportDot(String fileName) 
	{
		final String path = fileName;
		final File file =new File(path); 
		try {
			// Creation du fichier
			file .createNewFile();
			// creation d'un writer (un écrivain)
			final FileWriter writer = new FileWriter(file);
			try 
			{
				//writer.write("ceci est un texte\n");
				writer.write("graph G {\n");
				for (Node n : model.getNodes()) {
					writer.write(n.exportNodeDot()+"\n");
				}
				for (Edge e : model.getEdges()) {
					writer.write(e.exportEdgeDot()+"\n");
				}
				writer.write("}"+"\n");
			} 
			finally 
			{
				writer.close();
			}
		} catch (Exception e) 
		{
			System.out.println("Impossible de creer le fichier");
		}

	}
	public void importDot(String fileName) 
	{
		LinkedList<String> listNode=new LinkedList<String>() ;
		LinkedList<String> listEdge=new LinkedList<String>() ;
		LinkedList<String> listString=new LinkedList<String>() ;
		try{
			
			InputStream ips=new FileInputStream(fileName); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String ligne;
			while ((ligne=br.readLine())!=null)
			{
				listString.add(ligne);
			}
			br.close(); 
		}
		catch (Exception e){
			System.out.println(e.toString());
		}
		for (String string : listString) 
		{
			if(string.contains(";"))
			{
				if(string.contains("--"))
				{
					listEdge.add(string);
				}
				else
				{
					listNode.add(string);
				}
			}
		}
		for (String string : listNode) {
			Node n=new Node(string);
			model.getNodes().add(n);
		}
		for (String string : listEdge) {
			Edge e=new Edge(string,model.getNodes());
			model.getEdges().add(e);
		}

	}


}
