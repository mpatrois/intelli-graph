package UnitTest;
import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import Model.Edge;
import Model.Node;


public class TestEdge {

	@Test
	public void testEdge() {
		Color c=Color.black;
		int thickness=3;
		int size=30;
		String typeShape="Rectangle";
		int x=20,y=23;
		Node n1=new Node(c, thickness, x, y, size, typeShape);
		Node n2=new Node(c, thickness, x+43, y+54, size, typeShape);
		Edge e=new Edge(c, thickness, n1, n2);
				
		assertEquals(c,e.getColor());
		assertEquals(thickness, e.getThickness());
		assertEquals(n1, e.getSource());
		assertEquals(n2, e.getTarget());
	}
}
