package UnitTest;
import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import Model.Node;


public class TestNode {

	
	@Test
	public void testNode() {
		Color c=Color.black;
		int thickness=3;
		int size=30;
		String typeShape="Rectangle";
		int x=20,y=23;
		Node n=new Node(c, thickness, x, y, size, typeShape);
		
		
		assertEquals(c,n.getColor());
		assertEquals(thickness, n.getThickness());
		assertEquals(size, n.getSize());
		assertEquals(typeShape, n.getTypeShape());
		assertEquals(x, n.getX());
		assertEquals(y, n.getY());
		
		n.move(45,67);
		
		assertEquals(n.getX(),45);
		assertEquals(n.getY(),67);
		
		n.setShape("Triangle");
		assertEquals(n.getTypeShape(),"Triangle");
		
		n.setSelected(true);
		assertEquals(n.isSelected(),true);
	}
	
}
