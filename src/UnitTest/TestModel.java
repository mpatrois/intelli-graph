package UnitTest;
import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.Test;

import Model.CopiedElements;
import Model.GraphModel;


public class TestModel {

	@Test 
	public void testGettersAndSetters()
	{
		CopiedElements elems=new CopiedElements();
		GraphModel model=new GraphModel(elems);
		
		Color c=Color.black;
		int thickness=3;
		int size=30;
		String typeShape="Triangle";
		boolean modeDeplacement=true;
		
		model.setDefaultColor(c);
		model.setDefaultTypeShape(typeShape);
		model.setSizeDefault(size);
		model.setThicknessDefault(thickness);
		model.setMoveMode(modeDeplacement);
		
		assertEquals(modeDeplacement, model.isInMoveMode());
		assertEquals(c, model.getColorDefault());
		assertEquals(size, model.getSizeDefault());
		assertEquals(typeShape, model.getTypeShape());
		assertEquals(thickness, model.getThicknessDefault());
		
	}
	@Test 
	public void testAddNodesAndEdges()
	{
		CopiedElements elems=new CopiedElements();
		GraphModel model=new GraphModel(elems);
		
		model.addNode(23, 34);
		model.addNode(32, 34);
		
		model.addEdge(model.getNodes().get(0),model.getNodes().get(1));
		
		assertEquals(model.getNodes().size(),2);
		assertEquals(model.getEdges().size(),1);
		
		model.addEdge(model.getNodes().get(0),model.getNodes().get(1));
		
	}

}
