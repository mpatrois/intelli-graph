package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.metal.MetalFileChooserUI;

import Model.CopiedElements;

public class IntelliGraph extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try 
				{
					ImageIcon loading = new ImageIcon(IntelliGraph.class.getResource("/images/graph.gif"));
					IntelliGraph frame = new IntelliGraph();
					frame.setVisible(true);
					frame.add(new JLabel(loading, JLabel.CENTER));
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public IntelliGraph() 
	{
		setTitle("IntelliGraph");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 600);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFichier = new JMenu("Fichier");
		menuBar.add(mnFichier);
		final CopiedElements elems=new CopiedElements();
		JMenuItem mntmNouveau = new JMenuItem("Nouveau");
		mntmNouveau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GraphFrame frame = new GraphFrame(elems);
			}
		});
		mnFichier.add(mntmNouveau);

		JMenuItem mntmCharger = new JMenuItem("Charger");
		mntmCharger.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				try{

					JFileChooser chooser = new JFileChooser();
					chooser.setDialogType(JFileChooser.OPEN_DIALOG);
					MetalFileChooserUI ui = (MetalFileChooserUI)chooser.getUI();
					java.lang.reflect.Field field = MetalFileChooserUI.class.getDeclaredField("fileNameTextField");
					field.setAccessible(true);
					JTextField tf = (JTextField) field.get(ui);
					tf.setEditable(false);

					chooser.setCurrentDirectory(new  File(""));


					FileTypeFilter graphmlFilter = new FileTypeFilter(".graphml", "Graphml Documents");
					FileTypeFilter dotFilter = new FileTypeFilter(".dot", "Dot Documents");

					chooser.removeChoosableFileFilter(chooser.getFileFilter());

					chooser.setFileFilter(dotFilter);
					chooser.setFileFilter(graphmlFilter);

					//Affichage et récupération de la réponse de l'utilisateur
					int reponse = chooser.showDialog(chooser,"Charger");

					// Si l'utilisateur clique sur OK
					if  (reponse == JFileChooser.APPROVE_OPTION)
					{

						if(chooser.getFileFilter().equals(graphmlFilter))
						{			                	
							String direct=chooser.getCurrentDirectory().getPath();
							String fileName=chooser.getSelectedFile().getName();
							GraphFrame frame = new GraphFrame(elems,direct,fileName,true);
							//setTitle(fileName);
						}
						if(chooser.getFileFilter().equals(dotFilter))
						{
							String direct=chooser.getCurrentDirectory().getPath();
							String fileName=chooser.getSelectedFile().getName();
							GraphFrame frame = new GraphFrame(elems,direct,fileName,false);
							//setTitle(fileName);
						}
					}
				}
				catch(HeadlessException he)
				{
					he.printStackTrace();
				} catch (NoSuchFieldException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		});
		mnFichier.add(mntmCharger);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
