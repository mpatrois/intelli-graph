package View;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Event;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JColorChooser;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import Controller.Controller;
import Model.Edge;
import Model.GraphModel;
import Model.Node;

public class GraphView extends JPanel implements Observer,MouseListener,MouseMotionListener,MouseWheelListener{

	GraphModel model;
	Controller controller;
	JMenuItem mntmCopier = new JMenuItem("Copier");
	JMenuItem mntmCouper = new JMenuItem("Couper");
	JMenuItem mntmColler = new JMenuItem("Coller");
	JMenuItem mntmSupprimer = new JMenuItem("Supprimer");
	JPopupMenu popupMenu = new JPopupMenu();
	Point2D.Double origin=new Point2D.Double(0,0);
	private double zoom=1;
	private Point mousePressPointOnScreen=new Point(0,0);
	private Point mousePressRealPoint=new Point(0,0);

	RectSelectView rectSelection=new RectSelectView();


	NodeViewMovable nodeClicked;
	boolean dragOut=false;
	boolean xyAxes=true;
	private Point endLine=new Point(0,0);
	LinkedList<NodeViewMovable> nodesViewMovable=new LinkedList<NodeViewMovable>();

	LinkedList<EdgeView> edgesView=new LinkedList<EdgeView>();
	private final JMenuItem mntmCouleurFond = new JMenuItem("Couleur fond");


	class RectSelectView
	{
		Rectangle rectangle=new Rectangle();
		boolean visible=false;

		void setRectSelect(Point p1,Point p2)
		{
			int x = Math.min(p1.x, p2.x);
			int y = Math.min(p1.y, p2.y);
			int width = Math.max(p1.x - p2.x, p2.x - p1.x);
			int height = Math.max(p1.y - p2.y, p2.y - p1.y);
			rectangle=new Rectangle(x,y,width,height);
			visible=true;
		}
		public void paint(Graphics2D g)
		{
			if(visible)
			{
				g.setColor(Color.green);
				g.draw(rectangle);
				g.setColor(new Color(0,255,0,60));
				g.fill(rectangle);
				g.setColor(model.getColorDefault());
			}
		}
	}


	public GraphView(GraphModel model_, Controller control)
	{
		super();
		this.setBackground(Color.white);
		this.model=model_;
		this.controller=control;
		this.controller.setGrapheView(this);
		this.model.addObserver(this);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addMouseWheelListener(this);
		popupMenu.setFocusable(false);
		add(popupMenu);
		popupMenu.add(mntmCopier);
		popupMenu.add(mntmCouper);
		popupMenu.add(mntmColler);
		JSeparator separator = new JSeparator();
		popupMenu.add(separator);
		popupMenu.add(mntmSupprimer);

		mntmCouleurFond.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color c = JColorChooser.showDialog(null, "Choose a Color", Color.white);
				if(c!=null)
				{
					setBackground(c);
				}
			}
		});

		popupMenu.add(mntmCouleurFond);

		mntmCouper.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(model.getSelection().isElementInSelection())
				{
					controller.couper();
					repaint();
				}
			}
		});

		mntmCopier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(model.getSelection().isElementInSelection())
				{
					controller.copier();
					repaint();
				}
			}
		});

		mntmColler.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				controller.coller();
			}
		});

		mntmSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controller.supprimerElementsSelectionnes();
			}
		});
	}
	public void paint(Graphics g)
	{
		super.paint(g);
		Graphics2D g2=(Graphics2D)g;
		g2.translate(origin.x,origin.y);
		g2.scale(zoom, zoom);


		g2.setRenderingHint((RenderingHints.KEY_ANTIALIASING),
				RenderingHints.VALUE_ANTIALIAS_ON);

		//Paint les axes
		if(xyAxes)
			paintXYaxes(g);

		//Paint les noeuds qui ne sont pas movables (par conséquent les noeuds non sélectionnés
		for (Node n : model.getNodes()) 
		{
			if(!n.isSelected())
			{
				NodeView nod=new NodeView(n);
				nod.drawNode(g2);
			}
		}
		//Paint les noeuds qui sont movables
		for (NodeViewMovable n : nodesViewMovable) 
		{
			n.drawNode(g2);
		}

		//Paint les aretes
		for (EdgeView n : edgesView) 
		{
			n.drawEdgeFalse(g2);
		}

		//Paint le rectangle de selection
		rectSelection.paint(g2);

		//Si le noeud selectionné existe ( le noeud sur lequel on a cliqué en dernier)
		if(nodeClicked!=null ) 
		{
			nodeClicked.drawNode(g2);
		}
		Node somTmp=model.getNodeTmp();

		//si le sommet source existe et la ligne aussi
		//on dessine une ligne qui sera peut etre une future arete
		if(somTmp!=null && endLine!=null)
		{
			g2.setColor(model.getColorDefault());
			g2.setStroke(new BasicStroke(model.getThicknessDefault()));
			g.drawLine((int)(somTmp.centerNode().x),
					(int)(somTmp.centerNode().y), 
					(int)(endLine.x),
					(int)(endLine.y));

			g2.setStroke(new BasicStroke(1));
		}

	}
	//Dessine les axes
	private void paintXYaxes(Graphics g) {
		g.setColor(Color.black);
		Point Orig=getRealPoint(origin);
		Point SE=getRealPoint(new Point(this.getWidth(),this.getHeight()));
		Point NO=getRealPoint(new Point(0,0));

		g.drawLine(Orig.x,Orig.y,Orig.x+SE.x,Orig.y);
		g.drawLine(NO.x,Orig.y,Orig.x,Orig.y);
		g.drawLine(Orig.x,Orig.y,Orig.x,Orig.x+NO.y);
		g.drawLine(Orig.x,Orig.y,Orig.x,Orig.x+SE.y);


		for (int i = Orig.x; i <Orig.x+SE.x ; i+=10) {
			g.drawLine(i,Orig.y+2,i,-(2));
		}
		for (int i = Orig.x; i >(NO.x-Orig.x) ; i-=10) {
			g.drawLine(i,Orig.y+2,i,-(2));
		}

		for (int i = Orig.y; i <Orig.y+SE.y ; i+=10) {
			g.drawLine(Orig.x-2,i,Orig.x+2,i);
		}
		for (int i = Orig.y; i >Orig.y+NO.y ; i-=10) {
			g.drawLine(Orig.x-2,i,Orig.x+2,i);
		}
		g.drawString("0", Orig.x+2, Orig.y-2);
	}

	//retourne le vrai point sur le model
	private Point getRealPoint(Double p) 
	{
		return new Point((int)((p.x-origin.x)/zoom),(int)((p.y-origin.y)/zoom));
	}

	//retourne les vrais coordonnes sur le model du JPanel visible 
	//pour l'algo de random position afin que les sommets
	//se positionnes dans un endroit visible
	public Point[] getRealBounds()
	{
		Point hautGauche=getRealPoint(new Point(0,0));
		Point basDroite=getRealPoint(new Point(getWidth(),getHeight()));
		Point[] tabPa=new Point[2];
		tabPa[0]=hautGauche;
		tabPa[1]=basDroite;
		return tabPa;
	}
	@Override
	public void update(Observable o, Object arg) 
	{
		//ceci est le sommet sur lequel on clique 
		//en dernier on peut le deplacer sans qu'il soit forcement selectionné
		if(model.getNodeTmp()!=null)
			this.nodeClicked=new NodeViewMovable(model.getNodeTmp());
		else
			this.nodeClicked=null;

		nodesViewMovable.clear();
		
		//ajoute tous les noeuds qui sont selectionnés a la liste de noeud graphique 
		//deplacable
		for (Node n : model.getSelection().getNodesSelected()) 
		{
			NodeViewMovable nodeFal=new NodeViewMovable(n);
			nodesViewMovable.add(nodeFal);
		}

		//on ajoute le noeud cliqué dans la liste des noeuds déplacable
		if(nodeClicked!=null)
		{
			nodesViewMovable.add(nodeClicked);
		}
		edgesView.clear();
		//Rempli la liste des aretes graphique
		//avec des nodesViewMovable si les aretes en contiennent
		for (Edge e : model.getEdges()) 
		{
			NodeViewMovable source = null;
			NodeViewMovable target = null;

			for (NodeViewMovable nod : nodesViewMovable) 
			{
				if(e.getSource()==nod.getNode())
				{
					source=nod;
				}
				if(e.getTarget()==nod.getNode())
				{
					target=nod;
				}
			}
			EdgeView edg=new EdgeView(source,target,e);
			edgesView.add(edg);

		}
		repaint();
	}
	@Override
	public void mouseClicked(MouseEvent e) 
	{
		if(SwingUtilities.isLeftMouseButton(e))
		{
			controller.leftClick(getRealPoint(e.getPoint()),e.isControlDown());
		}
		else 
			if(SwingUtilities.isRightMouseButton(e))
			{
				controller.rightClick(getRealPoint(e.getPoint()));
				if(!model.getSelection().isElementInSelection())
				{
					mntmCopier.setEnabled(false);
					mntmCouper.setEnabled(false);
					mntmSupprimer.setEnabled(false);
				}
				else
				{
					mntmCopier.setEnabled(true);
					mntmCouper.setEnabled(true);
					mntmSupprimer.setEnabled(true);
				}
				if((model.getCopiedElement().sommetCopier.size()==0))
					mntmColler.setEnabled(false);
				else
					mntmColler.setEnabled(true);

				popupMenu.show(e.getComponent(), e.getX(), e.getY());

			}
	}

	public Point getRealPoint(Point p)
	{
		return new Point((int)((p.x-origin.x)/zoom),(int)((p.y-origin.y)/zoom));
	}
	public Point2D.Double getRealPointDouble(Point p)
	{
		return new Point2D.Double(((p.x-origin.x)/zoom*1.0),((p.y-origin.y)/zoom*1.0));
	}
	@Override
	public void mousePressed(MouseEvent arg0) {

		if(SwingUtilities.isLeftMouseButton(arg0))
		{
			mousePressPointOnScreen=arg0.getPoint();
			mousePressRealPoint=getRealPoint(arg0.getPoint());

			controller.mousePressed(mousePressRealPoint);

			if(!model.isInMoveMode() && arg0.isControlDown())
				endLine=getRealPoint(arg0.getPoint());
		}
		if(SwingUtilities.isMiddleMouseButton(arg0))
		{
			mousePressPointOnScreen=arg0.getPoint();
		}
	}
	@Override
	public void mouseReleased(MouseEvent arg0) 
	{
		if(SwingUtilities.isLeftMouseButton(arg0))
		{

			if(!model.isInMoveMode())	
				controller.mouseReleasedToAddEdge(getRealPoint(arg0.getPoint()));
			else
			{
				if(!nodesViewMovable.isEmpty())
				{
					if(nodesViewMovable.getFirst().hadMove())
						controller.moveNodesForReal(nodesViewMovable);
				}				
			}

			if(rectSelection.visible)
				rectSelection.visible=false;

			endLine=null;

			repaint();

		}
		dragOut=false;
	}
	public class ThreadForDragOut extends Thread {

		int dx=0;
		int dy=0;
		Point pt;
		public ThreadForDragOut(Point point) {

			if(point.x>getWidth())
				dx=1;
			else if(point.x<=0)
				dx=-1;

			if(point.y>getHeight())
				dy=1;
			else if(point.y<=0)
				dy=-1;

			pt=point;	  
		}
		public void run() {
			while(dragOut)
			{
				origin.x-=dx;
				origin.y-=dy;

				if(model.getNodeTmp()==null)
				{
					rectSelection.setRectSelect(mousePressRealPoint, getRealPoint(pt));
				}

				if(!model.isInMoveMode())
					endLine=getRealPoint(pt);

				repaint();

				try 
				{
					Thread.sleep (50);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}

	public boolean mouseOut(Point ptMouse)
	{
		return ptMouse.x>=getWidth() || ptMouse.x<=0 || ptMouse.y>= getHeight() || ptMouse.y<=0 ;
	}

	@Override
	public void mouseDragged(MouseEvent arg0) 
	{

		int dxOrigin=arg0.getX()-mousePressPointOnScreen.x;
		int dyOrigin=arg0.getY()-mousePressPointOnScreen.y;

		mousePressPointOnScreen=arg0.getPoint();
		if(SwingUtilities.isMiddleMouseButton(arg0) || (SwingUtilities.isLeftMouseButton(arg0) && arg0.isAltDown()))
		{
			origin.x+=dxOrigin;
			origin.y+=dyOrigin;
			repaint();
		}
		else if(SwingUtilities.isLeftMouseButton(arg0))
		{


			if(mouseOut(arg0.getPoint()))
			{
				dragOut=true;
				ThreadForDragOut thredDragOut = new ThreadForDragOut(arg0.getPoint()) ;
				thredDragOut.start();
			}
			else
			{
				dragOut=false;
			}

			if(nodeClicked==null)
			{
				rectSelection.setRectSelect(mousePressRealPoint, getRealPoint(arg0.getPoint()));
				controller.selectNodesOrEges(rectSelection.rectangle,arg0.isControlDown());
				repaint();
			}
			else
			{
				if(model.isInMoveMode())
				{
					endLine=null;

					if(!nodesViewMovable.isEmpty() && nodeClicked!=null)
					{
						moveAllNodesSelected(getRealPoint(arg0.getPoint()));
					}
				}
				else
				{
					endLine=getRealPoint(arg0.getPoint());

				}
				repaint();
			}
		}

	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
	}
	@Override
	public void mouseExited(MouseEvent arg0) {	
	}
	@Override
	public void mouseMoved(MouseEvent arg0) {
	}

	public double getZoom() {
		return zoom;
	}

	//set le zoom et l'origine en fonction du centre du jpanel
	public void setZoom(double zoom_) 
	{
		if(zoom_<=8 && zoom_>=0.25)
		{
			Point2D p1 = getRealPointDouble(new Point(getWidth()/2,getHeight()/2));
			zoom=zoom_;
			Point2D p2 = getRealPointDouble(new Point(getWidth()/2,getHeight()/2));
			origin.x+=p2.getX()*1.0 - p1.getX()*1.0;
			origin.y+=p2.getY()*1.0 - p1.getY()*1.0;
			repaint();
		}
	}
	private Point2D.Float transformPoint(Point p1,AffineTransform tf) throws NoninvertibleTransformException {
		AffineTransform inverse = tf.createInverse();
		Point2D.Float p2 = new Point2D.Float();
		inverse.transform(p1, p2);
		return p2;
	}

	//set le zoom et l'origine en fonction d'un point
	public void setZoomPoint(double newZoom,Point p) throws NoninvertibleTransformException 
	{
		if(newZoom<=8 && newZoom>=0.25)
		{

			AffineTransform fr=new AffineTransform();
			fr.translate(origin.x, origin.y);
			fr.scale(zoom, zoom);
			Point2D p1 = transformPoint(p,fr);

			AffineTransform fr2=new AffineTransform();
			fr2.translate(origin.x, origin.y);
			fr2.scale(newZoom, newZoom);

			Point2D p2 = transformPoint(p,fr2);

			origin.x+=p2.getX()*1.0 - p1.getX()*1.0;
			origin.y+=p2.getY()*1.0 - p1.getY()*1.0;

			zoom=newZoom;
			repaint();
		}
	}
	public void recenter() {

		origin=new Point2D.Double(this.getWidth()/2,this.getHeight()/2);
		repaint();
	}
	public void moveAllNodesSelected(Point pt)
	{
		Point ptTmp=nodeClicked.getNode().centerNode();
		for (NodeViewMovable s : nodesViewMovable) 
		{
			int dx=s.getNode().getX()-ptTmp.x;
			int dy=s.getNode().getY()-ptTmp.y;

			s.setX(pt.x+dx);
			s.setY(pt.y+dy);
		}
	}
	@Override
	public void mouseWheelMoved(MouseWheelEvent arg0) {
		if(arg0.isControlDown())
			if(arg0.getWheelRotation()>0)
			{
				try {
					setZoomPoint(zoom*1.05,arg0.getPoint());
				} catch (NoninvertibleTransformException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else
				try {
					setZoomPoint(zoom/1.05,getRealPoint(arg0.getPoint()));
				} catch (NoninvertibleTransformException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
}
