package View;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import Model.Edge;

/*
 * Represente les aretes sur la vue
 * si les noeuds de larete bouge , on dessine des traits en pointillé
 */
public class EdgeView {

	public NodeViewMovable source;
	public NodeViewMovable target;
	Edge edge;

	public EdgeView(NodeViewMovable source_, NodeViewMovable target_,Edge e) {
		source=source_;
		target=target_;
		edge=e;
	}
	public void drawEdgeFalse(Graphics2D g)
	{
		//dotted indique si l'arete va etre pointillée 
		//( uniquement si un de ses deux sommets ) bouge
		boolean dotted=false;
		if(source!=null)
		{
			dotted=source.hadMove();
		}
		if(!dotted && target !=null)
		{
			dotted=target.hadMove();
		}


		int x,y;//l'endroit d'ou part l'arete
		int x2,y2;//l'endroit ou elle arrive
		
		
		/*si la source est definie alors le debut du trait est au centre
		//du NodeView source */
		if(source!=null)
		{
			
			/* Ajustement du dessin en fonction de la forme du NodeViewMovable source */
			if(!source.getNode().getTypeShape().equals("Triangle"))
			{
				
				x=source.getX()+source.getNode().getSize()/2;
				y=source.getY()+source.getNode().getSize()/2;
			}
			
			else
			{
				x=source.getX();
				y=source.getY()+source.getNode().getSize();
			}
		}
		else
		{
			/*sinon le debut du trait est au centre
			//du Node source de Edge*/
			x=edge.getSource().centerNode().x;
			y=edge.getSource().centerNode().y;
		}
		
		
		
		/*meme traitement pour la destination
		*/
		if(target!=null)
		{
			if(!target.getNode().getTypeShape().equals("Triangle"))
			{
				x2=target.getX()+target.getNode().getSize()/2;
				y2=target.getY()+target.getNode().getSize()/2;
			}
			else
			{
				x2=target.getX();
				y2=target.getY()+target.getNode().getSize();
			}

		}
		else
		{
			x2=edge.getTarget().centerNode().x;
			y2=edge.getTarget().centerNode().y;
		}
		
		//Si un des deux noeuds a bougé de sa position initiale
		//on dessine le trait final 
		if(dotted)
		{

			Stroke s = new BasicStroke(2.0f, // Width
					BasicStroke.CAP_SQUARE,    // End cap
					BasicStroke.JOIN_MITER,    // Join style
					10.0f,                     // Miter limit
					new float[] {16.0f,20.0f}, // Dash pattern
					0.0f);    
			g.setStroke(s);
			g.drawLine(x,y,x2,y2);
		}

		g.setStroke(new BasicStroke(edge.getThickness()));

		//si l'arete est selectionnée elle est rouge
		if(edge.isSelected())
			g.setColor(Color.RED);
		else
			g.setColor(edge.getColor());


		//dans tous les cas on dessine l'arete originale
		g.drawLine(edge.getSource().centerNode().x,edge.getSource().centerNode().y,
				edge.getTarget().centerNode().x,edge.getTarget().centerNode().y);


		g.setStroke(new BasicStroke(1));
		g.setColor(Color.black);
		float xString=((edge.getSource().getX())+(edge.getTarget().getX()))/2;
		float yString=((edge.getSource().getY())+(edge.getTarget().getY()))/2;

		g.drawString(edge.getName(),xString,yString);

		g.setStroke(new BasicStroke(1));

	}
}
