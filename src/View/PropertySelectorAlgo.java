package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JColorChooser;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;





import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Controller.Controller;
import javax.swing.SpinnerNumberModel;


public class PropertySelectorAlgo extends JDialog {

	private final JPanel contentPanel = new JPanel();
	/**
	 * @wbp.nonvisual location=52,239
	 */
	private boolean colorMode;
//	LinkedList<Sommet> listSommet;
//	LinkedList<Arete> listArete;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the dialog.
	 */
	public PropertySelectorAlgo(boolean mode,HashSet<String> hashSet,Controller control_) 
	{
		
		super();
		final Controller control=control_;
		colorMode=mode;
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		final JButton btnColor2 = new JButton("Couleur 2");
		btnColor2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Color c = JColorChooser.showDialog(null, "Choose a Color", btnColor2.getForeground());
				if(c!=null)
				{
					btnColor2.setBackground(c);
				}
			}
		});
		final JButton btnColor1 = new JButton("Couleur 1");
		
		
		JLabel lblMin = new JLabel("Min :");
		
		JLabel lblMax = new JLabel("Max:");
		
		final JSpinner spinnerMin = new JSpinner();
		spinnerMin.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		
		final JSpinner spinnerMax = new JSpinner();
		spinnerMax.setModel(new SpinnerNumberModel(new Integer(2), new Integer(2), null, new Integer(1)));
		
		
		
		if(colorMode)
		{
			lblMin.setVisible(false);
			lblMax.setVisible(false);
			spinnerMin.setVisible(false);
			spinnerMax.setVisible(false);
			btnColor2.setVisible(true);
			btnColor1.setVisible(true);
			
		}else
		{
			lblMin.setVisible(true);
			lblMax.setVisible(true);
			btnColor2.setVisible(false);
			btnColor1.setVisible(false);
		}
		
		JLabel lblProprite = new JLabel("Propriétée");
		
		
		btnColor1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Color c = JColorChooser.showDialog(null, "Choose a Color", btnColor1.getForeground());
				if(c!=null)
				{
					btnColor1.setBackground(c);
				}
			}
		});
		
		final JComboBox comboBox = new JComboBox();
		for (String string : hashSet) {
			comboBox.addItem(string);
		}
		
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnColor1)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnColor2, 0, 0, Short.MAX_VALUE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(19)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblProprite)
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
									.addComponent(lblMin)
									.addComponent(lblMax)))
							.addGap(18)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(spinnerMax, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
								.addComponent(spinnerMin, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
								.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE))
							.addGap(38)))
					.addContainerGap(232, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(24)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblProprite)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(42)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(spinnerMin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblMin))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(spinnerMax, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblMax))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnColor1)
						.addComponent(btnColor2))
					.addContainerGap(53, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() 
				{
					

					public void actionPerformed(ActionEvent e) {
						
							if(colorMode)
							{
								control.executeAlgorithmColor((String)comboBox.getSelectedItem(),btnColor1.getBackground(),btnColor2.getBackground());
							}
							else
							{
								control.executeAlgoTaille((String)comboBox.getSelectedItem(),(int)spinnerMin.getValue(),(int)spinnerMax.getValue());
							}
						
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			
		}
		
		

		
	}
	void showDialog() {
	    setVisible(true);
	}
}