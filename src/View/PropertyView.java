package View;



import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;

import javax.print.attribute.AttributeSet;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import Controller.Controller;
import Model.Edge;
import Model.GraphModel;
import Model.Node;

public class PropertyView extends JPanel implements Observer {
	
	private JTextField textFieldName= new JTextField();
	JSpinner spinnerThickness = new JSpinner();
	JSpinner spinnerSize = new JSpinner();
	JButton buttonCouleur = new JButton("");
	JComboBox comboBoxShape = new JComboBox();
	JLabel labelSize = new JLabel("Taille");
	JLabel labelShape = new JLabel("Forme");
	
	JLabel labelName = new JLabel("Etiquette");
	String name="";
	boolean changeOnlyModel=true;
	boolean canChange=false;
	GraphModel model;
	Controller controller;
	private JTable table;
	JScrollPane scrollPane = new JScrollPane();
	JButton btnSupprimeProprit = new JButton("Supprime propriété");
	private final JButton btnChangeNom = new JButton("Change étiquette");
	
	class JTextFieldLimit extends PlainDocument {
		  private int limit;
		private AttributeSet attr;
		private AttributeSet attr2;
		  JTextFieldLimit(int limit) {
		    super();
		    this.limit = limit;
		  }

		  JTextFieldLimit(int limit, boolean upper) {
		    super();
		    this.limit = limit;
		  }

		  public void insertString(int offset, String str, javax.swing.text.AttributeSet attr) throws BadLocationException {
		    
			if (str == null)
		      return;

		    if ((getLength() + str.length()) <= limit) {
		      super.insertString(offset, str, attr);
		    }
		  }
		}
	
	/**
	 * Create the panel.
	 */
	public PropertyView(GraphModel model_,Controller controller_) {
		btnChangeNom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int row=table.getSelectedRow();
				int col=table.getSelectedColumn();
				if(row>-1 && col >-1)
				{
					String nameProp=(String)table.getModel().getValueAt(row, 0);
					Double valueProp=(Double)(table.getModel().getValueAt(row, 1));
					controller.changeNameOfElement(nameProp+"("+valueProp.toString()+")");
				}
			}
		});
		setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		setBackground(UIManager.getColor("Button.background"));
		
		JLabel label = new JLabel("Couleur");
		
		JLabel label_1 = new JLabel("Epaisseur");
		
		model=model_;
		model.getSelection().addObserver(this);
		controller=controller_;
		
		
		buttonCouleur.setBackground(Color.BLACK);
		buttonCouleur.setActionCommand("OK");
		
		buttonCouleur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color c = JColorChooser.showDialog(null, "Choose a Color", buttonCouleur.getForeground());
				if(c!=null)
				{
					buttonCouleur.setBackground(c);
					controller.setColor(c,changeOnlyModel);
				}
			}
		});

		
		
		textFieldName.setDocument(new JTextFieldLimit(10));
	
		
		textFieldName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{	
					controller.setNameToSelectedElement(textFieldName.getText());
			}
		});
		
		textFieldName.setColumns(10);
		spinnerThickness.setModel(new SpinnerNumberModel(2, 1, 5, 1));
		
		spinnerThickness.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) 
			{
				
				int value=(int) spinnerThickness.getValue();
				controller.setThickness(value,changeOnlyModel);
			}
		});
		spinnerSize.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) 
			{
				int value=(int) spinnerSize.getValue();
				controller.setSize(value,changeOnlyModel);
			}
		});
		spinnerSize.setModel(new SpinnerNumberModel(20, 2, 100, 1));
		comboBoxShape.setFocusable(false);
		
		comboBoxShape.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				controller.setTypeShape((String)comboBoxShape.getSelectedItem(),changeOnlyModel);
			}
		});
		comboBoxShape.setModel(new DefaultComboBoxModel(new String[] {"Rectangle", "Triangle", "Rond"}));
		
		
		
		

		//TableModelListener tcl = new TableModelListener(table, null);
	
		
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(10)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(labelName)
											.addPreferredGap(ComponentPlacement.RELATED))
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
											.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
												.addComponent(labelSize, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(label_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
											.addGroup(groupLayout.createSequentialGroup()
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(labelShape, GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)))))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(9)
									.addComponent(label, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)))
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(buttonCouleur, GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
								.addComponent(spinnerThickness, GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
								.addComponent(textFieldName, GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
								.addComponent(comboBoxShape, 0, 229, Short.MAX_VALUE)
								.addComponent(spinnerSize, GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnSupprimeProprit))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnChangeNom)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(8)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(buttonCouleur, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(label))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(9)
							.addComponent(label_1))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(6)
							.addComponent(spinnerThickness, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(9)
							.addComponent(labelSize))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(6)
							.addComponent(spinnerSize, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(9)
							.addComponent(labelShape))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(6)
							.addComponent(comboBoxShape, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(9)
							.addComponent(labelName))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(6)
							.addComponent(textFieldName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnSupprimeProprit)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnChangeNom)
					.addContainerGap(79, Short.MAX_VALUE))
		);
		btnSupprimeProprit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row=table.getSelectedRow();
				int col=table.getSelectedColumn();
				if(row>-1 && col >-1)
				{
					Object o=table.getModel().getValueAt(row, 0);
					controller.removePropertyOfElement((String)o);
				}
			}
		});
		
		table = new JTable();
		
		
		
		
		scrollPane.setViewportView(table);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
			}
		));
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//		table.setColumnSelectionAllowed(false);
//		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
//		    public void valueChanged(ListSelectionEvent e) {
//		       
//		    	int row = table.getSelectedRow();
//		    	textField.setText((String) table.getModel().getValueAt(table.getSelectedRow(), 0));
//		    }
//		});
		setLayout(groupLayout);
		changePropertyView();

	}
	void changePropertyView()
	{
		class TableauModif implements TableModelListener{

			@Override
			public void tableChanged(TableModelEvent arg0) {
				//System.out.println(arg0.getColumn());
				int row=table.getSelectedRow();
				int col=table.getSelectedColumn();
				if(row>-1 && col >-1)
				{
					
					String key=(String) table.getModel().getValueAt(row, 0);
					String value=table.getModel().getValueAt(row, 1).toString();
					if(key!="" && value!="" )
					controller.addPropertyToElement(key, Double.parseDouble(value));
				}
			}
		
		    
		};
		scrollPane.setVisible(false);
		btnSupprimeProprit.setVisible(false);
		btnChangeNom.setVisible(false);
		//spinnerSize.setValue(model.getTailleDefaut());
	//	spinnerThickness.setValue(model.getEpaisseurDefaut());
		buttonCouleur.setBackground(model.getColorDefault());
		comboBoxShape.setToolTipText(model.getTypeShape());
		
		labelShape.setVisible(true);
		labelName.setVisible(true);
		labelSize.setVisible(true);
		comboBoxShape.setVisible(true);
		textFieldName.setVisible(false);
		labelName.setVisible(false);
		spinnerSize.setVisible(true);
		changeOnlyModel=true;
		
		LinkedList<Node> listSommet=model.getSelection().getNodesSelected();
		LinkedList<Edge> listArete=model.getSelection().getEdgesSelected();
		
		//si il y a des sommets et des aretes 
		if ( (listSommet.size()>0 && listArete.size()>0) || listArete.size()>1  )
		{
			
			labelShape.setVisible(false);
			labelName.setVisible(false);
			labelSize.setVisible(false);
			spinnerSize.setVisible(false);
			comboBoxShape.setVisible(false);
			textFieldName.setVisible(false);
			changeOnlyModel=false;
		}
		//si il n'y a que des sommets
		else if (listSommet.size()>1 && listArete.size()==0 )
		{
			
			
			changeOnlyModel=false;
	
		}
		//si il n'y a qun sommet
		else if (listSommet.size()==1 && listArete.size()==0)
		{
			
			textFieldName.setVisible(true);
			labelName.setVisible(true);
			
			Node tmp=listSommet.getFirst();
			textFieldName.setText(tmp.getName());
			spinnerSize.setValue(tmp.getSize());
			spinnerThickness.setValue(tmp.getThickness());
			buttonCouleur.setBackground(tmp.getColor());
			comboBoxShape.setToolTipText(tmp.getTypeShape());
			changeOnlyModel=false;
			
			
			Node n=listSommet.getFirst();
			DefaultTableModel modelTableau = new DefaultTableModel(); 
			
			modelTableau.addColumn("Attribut");
			modelTableau.addColumn("Valeur");
			
			//modelTableau.addRow(new Object[]{"Attribut","Valeur"});
			
			for(Entry<String, Double> entry : n.getNodeAttributes().entrySet()) 
			{
				modelTableau.addRow(new Object[]{entry.getKey(),entry.getValue()});
			}
			modelTableau.addRow(new Object[]{"",""});
			table.setModel(modelTableau);
			table.getModel().addTableModelListener(new TableauModif());
			
			int rowIndex = 0;
			int columnIndex = 1;
			boolean includeSpacing = true;
			 
			Rectangle cellRect = table.getCellRect(rowIndex, columnIndex, includeSpacing);
			 
			table.scrollRectToVisible(cellRect);
			btnSupprimeProprit.setVisible(true);
			btnChangeNom.setVisible(true);
			

			scrollPane.setVisible(true);
		}
		//si il n'y a qune arete
		else if (listSommet.size()==0 && listArete.size()==1)
		{
			
			textFieldName.setVisible(true);
			labelName.setVisible(true);
			
			labelSize.setVisible(false);
			spinnerSize.setVisible(false);
			
			labelShape.setVisible(false);
			comboBoxShape.setVisible(false);
			
			Edge tmp=listArete.getLast();
			textFieldName.setText(tmp.getName());
			spinnerThickness.setValue(tmp.getThickness());
			buttonCouleur.setBackground(tmp.getColor());
			changeOnlyModel=false;
			
			
			btnSupprimeProprit.setVisible(true);
			btnChangeNom.setVisible(true);
			DefaultTableModel modelTableau = new DefaultTableModel(); 
			
			modelTableau.addColumn("Attribut");
			modelTableau.addColumn("Valeur");
			
			//modelTableau.addRow(new Object[]{"Attribut","Valeur"});
			
			for(Entry<String, Double> entry : tmp.getEdgeAttributes().entrySet()) 
			{
				modelTableau.addRow(new Object[]{entry.getKey(),entry.getValue()});
			}
			modelTableau.addRow(new Object[]{"",""});
			table.setModel(modelTableau);
			table.getModel().addTableModelListener(new TableauModif());
			
			int rowIndex = 0;
			int columnIndex = 1;
			boolean includeSpacing = true;
			 
			Rectangle cellRect = table.getCellRect(rowIndex, columnIndex, includeSpacing);
			 
			table.scrollRectToVisible(cellRect);

			
//			textField.setVisible(true);
//			spinner.setVisible(true);
//			btnAjoutProprit.setVisible(true);
			scrollPane.setVisible(true);
		}

	}
	@Override
	public void update(Observable o, Object arg) {
		this.changePropertyView();
		repaint();
	}
}
