package View;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

import Model.Node;
import Model.Triangle;

public class NodeView {
	
	//le sommet a dessiné
	private Node node;
	
	public NodeView(Node n) {
		node=n;
	}
	
	public void drawNode(Graphics2D g2D)
	{
			/*On change l"épaisseur du trait */
			g2D.setStroke(new BasicStroke(getNode().getThickness()));

			/*Si le sommet est selectionné on l'affiche rouge */
			if (getNode().isSelected())
			{
				g2D.setColor(Color.RED);
			}
			else
				g2D.setColor(getNode().getColor());


			/*Dessiner la forme du sommet */
			if(getNode().getTypeShape()=="Rectangle")
			{
				g2D.fillRect(getNode().getX(),getNode().getY(),getNode().getSize(),getNode().getSize());
				g2D.setColor(Color.black);
				g2D.drawRect(getNode().getX(),getNode().getY(),getNode().getSize(),getNode().getSize());
			}
			else if(getNode().getTypeShape()=="Rond")
			{
				g2D.fillOval(getNode().getX(),getNode().getY(),getNode().getSize(),getNode().getSize());
				g2D.setColor(Color.black);
				g2D.drawOval(getNode().getX(),getNode().getY(),getNode().getSize(),getNode().getSize());
			}
			else if(getNode().getTypeShape()=="Triangle")
			{
				Triangle nodeTmp=(Triangle) getNode().getShape();
				
				Polygon p=new Polygon();
				
				p.addPoint(nodeTmp.getX(),nodeTmp.getY());
				p.addPoint(nodeTmp.getGauche().x,nodeTmp.getGauche().y);
				p.addPoint(nodeTmp.getDroite().x,nodeTmp.getDroite().y);
				
				
				g2D.fill(p);
				g2D.setColor(Color.black);
				g2D.draw(p);
			}

			/*On remet l'épaisseur à une taille normale */
			g2D.setStroke(new BasicStroke(1));
			
			/*On dessine le nom du sommet */
			g2D.drawString(getNode().getName(),getNode().getX(), getNode().getY()-getNode().getThickness()/2);
		}

	public Node getNode() {
		return node;
	}
	
}

