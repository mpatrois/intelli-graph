package View;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;

import Model.Node;
import Model.Triangle;

public class NodeViewMovable extends NodeView {

	private int x;
	private int y;

	//Represente un Noeud graphique qui peut être déplacé
	public NodeViewMovable(Node n) {
		super(n);
		setX(n.getX());
		setY(n.getY());
	}

	//Dessine le noeud 
	public void drawNode(Graphics2D g2D)
	{
		//on dessine le noeud a sa position originale
		super.drawNode(g2D);


		//si il bouge on en dessine un autre à sa nouvelle pos
		if(getX()!=getNode().getX() || getY()!=getNode().getY())
		{
			g2D.setStroke(new BasicStroke(getNode().getThickness()));
			g2D.setColor(Color.blue);

			/*Dessiner la forme du sommet */
			if(getNode().getTypeShape()=="Rectangle")
			{
				g2D.fillRect(getX(),getY(),getNode().getSize(),getNode().getSize());
				g2D.setColor(Color.black);
				g2D.drawRect(getX(),getY(),getNode().getSize(),getNode().getSize());
			}
			else if(getNode().getTypeShape()=="Rond")
			{
				g2D.fillOval(getX(),getY(),getNode().getSize(),getNode().getSize());
				g2D.setColor(Color.black);
				g2D.drawOval(getX(),getY(),getNode().getSize(),getNode().getSize());
			}
			else if(getNode().getTypeShape()=="Triangle")
			{
				int tail=getNode().getSize();
				Polygon p=new Polygon();

				Point haut=new Point(getX(),getY());
				Point droite=new Point(getX()+tail,getY()+tail/2+tail);
				Point gauche=new Point(getX()-tail,getY()+tail/2+tail);

				p.addPoint(haut.x,haut.y);
				p.addPoint(gauche.x,gauche.y);
				p.addPoint(droite.x,droite.y);

				g2D.fill(p);
				g2D.setColor(Color.black);
				g2D.draw(p);
			}

			/*On remet l'épaisseur à une taille normale */
			g2D.setStroke(new BasicStroke(1));

			/*On dessine le nom du sommet */
			g2D.drawString(getNode().getName(),getX(),getY()-getNode().getThickness()/2);
		}

	}
	public Point getPosition()
	{
		return new Point(getX(),getY());
	}
	public void moveNodeViewMovable(Point pt)
	{
		this.setX(pt.x-getNode().getSize()/2);
		this.setY(pt.y-getNode().getSize()/2);
	}
	public boolean hadMove()
	{
		return (getX()!=getNode().getX() || getY()!=getNode().getY());
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
}
