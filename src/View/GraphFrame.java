package View;

import java.awt.Button;
import java.awt.Component;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Observable;
import java.util.Observer;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.plaf.metal.MetalFileChooserUI;

import Controller.Controller;
import Model.CopiedElements;
import Model.GraphModel;



public class GraphFrame extends JFrame implements Observer
{

	JButton btnSupprimer = new JButton("Supprimer");
	JButton buttonRedo = new JButton("");
	private JButton buttonUndo = new JButton("");
	final JMenuItem mntmEnregistrer = new JMenuItem("Enregistrer");

	JToggleButton tglbtnDplacer = new JToggleButton("Deplacer");
	JSpinner spinner = new JSpinner();
	JSpinner spinner_1 = new JSpinner();
	Button button = new Button("");
	JToggleButton tglbtnDplacement = new JToggleButton("Mode");
	boolean saved=false;
	private Controller controller;
	private GraphModel model;

	/**
	 * Le graphe sera enregistr� sous ce nom de fichier
	 */
	private String nameFile="";

	/**
	 * Le graphe sera enregistré dans ce dossier
	 */
	private String directory="";


	/**
	 * Create the frame.
	 * @param elems 
	 */
	public GraphFrame(final CopiedElements elems) {

		super();

		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {
            	
            	if(mntmEnregistrer.isEnabled())
    				{
    					int result = JOptionPane.showConfirmDialog((Component) null, "Vous êtes sur le point de quitter, voulez vous enregistrer ? ","Quitter ?", JOptionPane.YES_NO_CANCEL_OPTION);
    					if(result==JOptionPane.NO_OPTION)
    					{
    						dispose();
    					}
    					else if(result==JOptionPane.YES_OPTION)
    					{
    						save();
    					}
    				}
            		else
            		dispose();
    			}
        });
	
		model=new GraphModel(elems);
		controller=new Controller(model);
		final GraphView graphView = new GraphView(model,controller);
		model.addObserver(this);
		setTitle("Document Vide");


		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		buttonRedo.setEnabled(false);
		buttonRedo.setBorder(new CompoundBorder());
		buttonRedo.setIcon(new ImageIcon(GraphFrame.class.getResource("/images/redo18.png")));

		buttonRedo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controller.redo();
			}
		} );
		buttonUndo.setEnabled(false);
		getButtonUndo().setBorder(new CompoundBorder());
		getButtonUndo().setIcon(new ImageIcon(GraphFrame.class.getResource("/images/undo18.png")));

		getButtonUndo().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controller.undo();
			}
		} );
		tglbtnDplacement.setBorder(null);
		tglbtnDplacement.setActionCommand("");
		tglbtnDplacement.setToolTipText("");
		tglbtnDplacement.setSelectedIcon(new ImageIcon(GraphFrame.class.getResource("/images/drag1.png")));
		tglbtnDplacement.setIcon(new ImageIcon(GraphFrame.class.getResource("/images/pen29.png")));


		tglbtnDplacement.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				controller.setModeDeplacement(tglbtnDplacement.isSelected());
			}
		});
		toolBar.add(tglbtnDplacement);

		JButton button_3 = new JButton("Supprimer");
		button_3.setBorder(null);
		button_3.setIcon(new ImageIcon(GraphFrame.class.getResource("/images/garbage21.png")));

		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.supprimerElementsSelectionnes();
			}
		});
		toolBar.add(button_3);



		JButton button_1 = new JButton("");
		button_1.setBorder(null);
		button_1.setIcon(new ImageIcon(GraphFrame.class.getResource("/images/zoom.png")));
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				graphView.setZoom(graphView.getZoom()*1.1);


			}
		});
		toolBar.add(button_1);

		JButton button_2 = new JButton("");
		button_2.setBorder(null);
		button_2.setIcon(new ImageIcon(GraphFrame.class.getResource("/images/dezoom.png")));
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				graphView.setZoom(graphView.getZoom()/1.1);
				graphView.repaint();
			}
		});
		toolBar.add(button_2);
		toolBar.add(getButtonUndo());

		toolBar.add(buttonRedo);


		graphView.setBounds(new Rectangle(0, 0, 300, 300));

		PropertyView propertyView = new PropertyView(model,controller);
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(toolBar, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
								.addComponent(graphView, GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(propertyView, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE)
								.addContainerGap())
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(propertyView, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addContainerGap())
										.addComponent(graphView, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)))
				);

		final JCheckBox chckbxAxes = new JCheckBox("Axes");
		chckbxAxes.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		chckbxAxes.setSelected(true);
		chckbxAxes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				graphView.xyAxes=chckbxAxes.isSelected();
				graphView.repaint();
			}
		});

		toolBar.add(chckbxAxes);

		JButton btnRecentrer = new JButton("Recentrer");
		btnRecentrer.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		btnRecentrer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				graphView.recenter();
			}
		});
		toolBar.add(btnRecentrer);

		getContentPane().setLayout(groupLayout);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFichier = new JMenu("Fichier");
		menuBar.add(mnFichier);

		JMenuItem mntmNouveau = new JMenuItem("Nouveau");
		mntmNouveau.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				GraphFrame frame = new GraphFrame(model.getCopiedElement());
			}
		});
		mnFichier.add(mntmNouveau);

		JMenuItem mntmCharger = new JMenuItem("Charger");
		mntmCharger.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) {
				try{

					JFileChooser chooser = new JFileChooser();
					chooser.setCurrentDirectory(new  File(""));


					MetalFileChooserUI ui = (MetalFileChooserUI)chooser.getUI();
					java.lang.reflect.Field field = MetalFileChooserUI.class.getDeclaredField("fileNameTextField");
					field.setAccessible(true);
					JTextField tf = (JTextField) field.get(ui);
					tf.setEditable(false);


					FileTypeFilter graphmlFilter = new FileTypeFilter(".graphml", "Graphml Documents");
					FileTypeFilter dotFilter = new FileTypeFilter(".dot", "Dot Documents");

					chooser.removeChoosableFileFilter(chooser.getFileFilter());

					chooser.setFileFilter(dotFilter);
					chooser.setFileFilter(graphmlFilter);

					//Affichage et récupération de la réponse de l'utilisateur
					int reponse = chooser.showDialog(chooser,"Charger");

					// Si l'utilisateur clique sur OK
					if  (reponse == JFileChooser.APPROVE_OPTION)
					{

						if(chooser.getFileFilter().equals(graphmlFilter))
						{			                	
							String direct=chooser.getCurrentDirectory().getPath();
							String fileName=chooser.getSelectedFile().getName();
							GraphFrame frame = new GraphFrame(elems,direct,fileName,true);
						}
						if(chooser.getFileFilter().equals(dotFilter))
						{
							String direct=chooser.getCurrentDirectory().getPath();
							String fileName=chooser.getSelectedFile().getName();
							GraphFrame frame = new GraphFrame(elems,direct,fileName,false);
						}
					}
				}
				catch(HeadlessException he)
				{
					he.printStackTrace();
				} catch (NoSuchFieldException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		});
		mnFichier.add(mntmCharger);


		mntmEnregistrer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				save();
			}
		});
		mntmEnregistrer.setEnabled(false);
		mnFichier.add(mntmEnregistrer);

		JMenuItem mntmEnregistrerSous = new JMenuItem("Enregistrer Sous");
		mntmEnregistrerSous.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				saveAs();
			}

			

		});
		mnFichier.add(mntmEnregistrerSous);

		JMenu mnAlgorithmes = new JMenu("Algorithmes");
		menuBar.add(mnAlgorithmes);

		JMenuItem mntmColorisation = new JMenuItem("Colorisation");
		mntmColorisation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controller.executeChromaticAlgorithm();
			}
		});

		JMenuItem mntmCouleur = new JMenuItem("Couleur");
		mntmCouleur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PropertySelectorAlgo fp=new PropertySelectorAlgo(true,model.getAllProperties(),controller);
				fp.setModal(true);
				fp.showDialog();
			}
		});

		JMenuItem mntmCalculDesDegrs = new JMenuItem("Calcul des degrés");
		mntmCalculDesDegrs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				String msg="La propriétée 'Degrée sommet' sera ajoutée à tous les sommets \n avec le "
						+ "nombre de leurs voisin respectifs";

				int result = JOptionPane.showConfirmDialog((Component) null, msg,"alert", JOptionPane.OK_CANCEL_OPTION);
				if(result==JOptionPane.OK_OPTION)
					controller.executeAlgorithmDegree();


			}
		});
		
		JMenuItem mntmAlatoire = new JMenuItem("Aléatoire");
		mntmAlatoire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controller.executeAlgorithmRandom();
			}
		});
		mnAlgorithmes.add(mntmAlatoire);
		mnAlgorithmes.add(mntmCalculDesDegrs);
		mnAlgorithmes.add(mntmCouleur);
		mnAlgorithmes.add(mntmColorisation);

		JMenuItem mntmLayoutCirculaire = new JMenuItem("Layout Circulaire");
		mntmLayoutCirculaire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.executeAlgoCirc();
			}
		});
		mnAlgorithmes.add(mntmLayoutCirculaire);

		JMenuItem mntmTaille = new JMenuItem("Taille");
		mntmTaille.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PropertySelectorAlgo fp=new PropertySelectorAlgo(false,model.getAllProperties(),controller);
				fp.setModal(true);
				fp.showDialog();
			}
		});
		mnAlgorithmes.add(mntmTaille);

		this.setVisible(true);
		this.setBounds(100,100, 610, 528);

		graphView.recenter();

	}
	private void saveAs() {
		try
		{ 
			JFileChooser chooser = new JFileChooser();

			chooser.setCurrentDirectory(new  File(""));


			FileTypeFilter graphmlFilter = new FileTypeFilter(".graphml", "Graphml Documents");
			FileTypeFilter dotFilter = new FileTypeFilter(".dot", "Dot Documents");

			chooser.removeChoosableFileFilter(chooser.getFileFilter());

			chooser.setFileFilter(dotFilter);
			chooser.setFileFilter(graphmlFilter);


			//Affichage et récupération de la réponse de l'utilisateur
			int reponse = chooser.showDialog(chooser,"Enregistrer sous");

			// Si l'utilisateur clique sur OK
			if  (reponse == JFileChooser.APPROVE_OPTION)
			{
				if(chooser.getFileFilter().equals(graphmlFilter))
				{			      
					directory=chooser.getCurrentDirectory().getPath();
					nameFile=chooser.getSelectedFile().getName()+".graphml";
					String fileToImport=directory+"\\"+nameFile;
					controller.exportGraphml(fileToImport);

					setTitle(nameFile);
				}
				if(chooser.getFileFilter().equals(dotFilter))
				{
					directory=chooser.getCurrentDirectory().getPath();
					nameFile=chooser.getSelectedFile().getName()+".dot";
					String fileToImport=directory+"\\"+nameFile;
					controller.exportDot(fileToImport);

					setTitle(nameFile);
				};
				mntmEnregistrer.setEnabled(true);
			}

		}
		catch(HeadlessException he)
		{
			he.printStackTrace();
		}
	}


	public GraphFrame(CopiedElements elems, String fileDirectory, String fileName_, boolean graphml) {
		this(elems);
		directory=fileDirectory;
		nameFile=fileName_;
		if(graphml)
			controller.importGraphml(directory+"\\"+nameFile);
		else
			controller.importDot(directory+"\\"+nameFile);
		setTitle(fileName_);
		
		mntmEnregistrer.setEnabled(false);
		

	}

	public JButton getButtonUndo() {
		return buttonUndo;
	}

	public JButton getButtonRedo() {
		return buttonRedo;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		getButtonUndo().setEnabled(model.getUndoManager().canUndo());
		getButtonRedo().setEnabled(model.getUndoManager().canRedo());
		mntmEnregistrer.setEnabled(true);
	}
	private void save() {
		if(directory!="")
		{
		String extension=nameFile.split("\\.")[1];

		if(extension.equals("graphml"))
		{
			controller.exportGraphml(directory+"\\"+nameFile);
		}
		else if(extension.equals("dot"))
		{
			controller.exportDot(directory+"\\"+nameFile);
		}
		mntmEnregistrer.setEnabled(false);
		}
		else
		{
			saveAs();
		}
	}
}
