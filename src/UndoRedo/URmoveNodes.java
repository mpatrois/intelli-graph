package UndoRedo;

import java.awt.Point;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.swing.undo.AbstractUndoableEdit;

import Model.Node;


public class URmoveNodes extends AbstractUndoableEdit {
	
	HashMap<Node,Point> map=new HashMap<>();
	
	public URmoveNodes(HashMap<Node,Point> map_) {
		map=map_;
	}
	public void redo() {
		super.redo();
		for(Entry<Node, Point> entry : map.entrySet()) 
		{
		    Node node = entry.getKey();
		    Point newPosition = entry.getValue();
		    Point lastPos=new Point(node.getX(),node.getY());
		    node.move(newPosition.x,newPosition.y);
		    entry.getValue().x=lastPos.x;
		    entry.getValue().y=lastPos.y;
		}
		
	}

	public void undo() {
		super.undo();
		for(Entry<Node, Point> entry : map.entrySet()) 
		{
		    Node node = entry.getKey();
		    Point newPosition = entry.getValue();
		    Point lastPos=new Point(node.getX(),node.getY());
		    node.move(newPosition.x,newPosition.y);
		    entry.getValue().x=lastPos.x;
		    entry.getValue().y=lastPos.y;
		}
	}
}
