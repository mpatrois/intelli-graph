package UndoRedo;

import java.util.LinkedList;

import javax.swing.undo.AbstractUndoableEdit;

import Model.Edge;
import Model.Node;

public class URremoveElements extends AbstractUndoableEdit{
	LinkedList<Node> sommets;
	LinkedList<Edge> aretes;
	LinkedList<Node> nodes=new LinkedList<Node>();
	LinkedList<Edge> edges=new LinkedList<Edge>();

		public URremoveElements(LinkedList<Node> nodes_,LinkedList<Edge> edges_) 
		{
			
			nodes.addAll(nodes_);
	    	edges.addAll(edges_);
	    	
	    	
	    }
		
	    public void redo() {
	      super.redo();
	      
	      aretes.removeAll(edges);
	      sommets.removeAll(nodes);
	    }
	
	    public void undo() {
	      super.undo();
	      
	      for (Edge edge : edges) {
			edge.setSelected(false);
	      }

	      for (Node node : nodes) {
	  		node.setSelected(false);
	        }
	      
	      aretes.addAll(edges);
	      sommets.addAll(nodes);
	    }
	    public void setNodesAndEdgeOfGraph(LinkedList<Node>sommets_,LinkedList<Edge>aretes_)
	    {
	    	sommets=sommets_;
	    	aretes=aretes_;
	    }
	  
}