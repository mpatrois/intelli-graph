package UndoRedo;

import java.util.LinkedList;

import javax.swing.undo.AbstractUndoableEdit;

import Model.Edge;
import Model.Node;

public class URaddNode extends AbstractUndoableEdit 
{
    Node n;
	LinkedList<Node>sommets;
	LinkedList<Edge>aretes;
	
    public URaddNode(Node n_) {
      n = n_;
    }
    public String getPresentationName() {
      return "Square Addition";
    }
    public void redo() {
      super.redo();
      sommets.add(n);
    }
    public void undo() {
      super.undo();
     // n.setSelected(f);
      sommets.remove(n);
    }
    public void setSommetsEtAretes(LinkedList<Node>sommets_,LinkedList<Edge>aretes_)
    {
    	sommets=sommets_;
    	aretes=aretes_;
    }
  }