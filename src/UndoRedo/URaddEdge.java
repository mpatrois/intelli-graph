package UndoRedo;

import java.util.LinkedList;

import javax.swing.undo.AbstractUndoableEdit;

import Model.Edge;
import Model.Node;

public class URaddEdge extends AbstractUndoableEdit 
{
	LinkedList<Node>sommets;
	LinkedList<Edge>aretes;
	Edge e;
	
    public URaddEdge(Edge e_) {
      e = e_;
    }
    public void setSommetsEtAretes(LinkedList<Node>sommets_,LinkedList<Edge>aretes_)
    {
    	sommets=sommets_;
    	aretes=aretes_;
    }


    public void redo() {
      super.redo();
      aretes.add(e);
    }

    public void undo() {
      super.undo();
      aretes.remove(e);
    }
  }
