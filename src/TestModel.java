import static org.junit.Assert.*;

import java.awt.Color;

import junit.framework.Assert;

import org.junit.Test;

import Model.CopiedElements;
import Model.GraphModel;


public class TestModel {

	@Test
	public void test()
	{
		
	}
	@Test 
	public void testGettersAndSetters()
	{
		CopiedElements elems=new CopiedElements();
		GraphModel model=new GraphModel(elems);
		
		Color c=Color.black;
		int thickness=3;
		int size=30;
		String typeShape="Triangle";
		boolean modeDeplacement=true;
		
		model.setDefaultColor(c);
		model.setDefaultTypeShape(typeShape);
		model.setTailleDefaut(size);
		model.setEpaisseurDefaut(thickness);
		model.setModeDeplacement(modeDeplacement);
		
		assertEquals(modeDeplacement, model.isInMoveMode());
		assertEquals(c, model.getCouleurDefaut());
		assertEquals(size, model.getTailleDefaut());
		assertEquals(typeShape, model.getTypeShape());
		assertEquals(thickness, model.getEpaisseurDefaut());
		
	}

}
