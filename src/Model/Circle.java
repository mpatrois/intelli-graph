package Model;

/**
*
* <p>
*  Circle est une classe héritée de la classe Rectangle
*  ils ont exactement les mêmes attributs et méthodes ,seul le getTypeShape change.
* </p>
* @see Node
* @see Rectangle
* 
* @author Intelli'Graph team
* @version 1.0
* 
*/

public class Circle extends Rectangle
{
	/**
	 * Instancie un nouveau Rond.
     * 
     * @param x
     * 			  L'abscisse du Rond
     * @param y
     * 			  L'ordonnée du Rond
     * @param tail
     * 			  La taille du Rond
     */
	Circle(int x, int y, int tail) {
		super(x, y, tail);
	}

	@Override
	public String getTypeShape() {
		return "Rond";
	}
}
