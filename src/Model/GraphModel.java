package Model;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Map.Entry;

import javax.swing.event.UndoableEditEvent;
import javax.swing.undo.UndoManager;

import UndoRedo.URaddEdge;
import UndoRedo.URaddNode;
import UndoRedo.URmoveNodes;
import UndoRedo.URremoveElements;
import Algorithm.DegreeAlgorithm;
import Algorithm.CircularLayout;
import Algorithm.ChromaticAlgorithm;
import Algorithm.ColorAlgorithm;
import Algorithm.RandomAlgorithm;
import Algorithm.SizeAlgorithm;
import ImportExport.DotExportImport;
import ImportExport.GraphmlExportImport;
/**
 *
 * <p>
 * GrapheModel contiens les sommets et les aretes du graphe et effectue
 * les traitements. Les sommets peuvents prendre différente forme 
 * <ul>
 * <li>rectangle</li>
 * <li>rond</li>
 * <li>triangle</li>
 * </ul>
 * </p>
 * <p>
 * Des arêtes peuvent lier deux sommets entre eux.
 * 
 * Le graphe peut être exporté sous l'extension .dot et .graphml
 * 
 * A partir du graphe on peut choisir la couleur, la taille et l'épaisseur des
 * futurs sommets et arêtes
 * 
 * </p>
 * 
 * @author  Intelli'Graph team
 * @version 1.0
 * 
 */
public class GraphModel extends Observable {

	/**
	 * Point sur lequel on effectué le dernier clic droit
	 */
	private Point pointRight = null;
	/**
	 * La couleur des futurs sommets et arêtes
	 */
	private Color defaultColor;

	/**
	 * L'épaisseur des futurs sommets et arêtes
	 */
	private int thiknessDefaut;
	/**
	 * La taille des futurs sommets et arêtes
	 */
	private int sizeDefault;

	/**
	 * La liste des sommets du graphe
	 */
	private LinkedList<Node> nodesList;

	/**
	 * La liste des arêtes du graphe
	 */
	private LinkedList<Edge> edgesList;

	/**
	 * Le dernier sommet sur lequel on a cliqué
	 */
	private Node nodeTmp = null;

	/**
	 * La forme des futurs sommets
	 */
	private String typeShape;

	/**
	 * Si on est en mode déplacement ou création
	 */
	private boolean moveMode = false;

	/**
	 * Ceci est une classe qui permet de gerer les sommets et arêtes sélectionnées
	 */
	private Selection selection = null;

	/**
	 * Les éléments copiés de l'application
	 */
	private CopiedElements elementsCopied;

	/**
	 * L'undo/redo mangager de l'application,permet d'annuler/refaire des actions
	 */
	private UndoManager undoManager = null;
	
	/**
	 * Constructeur par défaut de la classe Graphe Cette méthode est appelée
	 * lors de la création de la fenêtre de l'application. Elle dérive de JPanel
	 * et implémente MouseListener et MouseMotionListener. Par défaut le nom du
	 * graphe pour la sauvegarde est "Document vide" la couleur des futurs
	 * sommets et arêtes noire, l'épaisseur est de 1 la taille par défaut 20 et
	 * il n' y a aucun sommet et aucune arête
	 */

	public GraphModel( CopiedElements elementsCopier_) 
	{
		typeShape="Rectangle";
		defaultColor = Color.ORANGE;
		thiknessDefaut = 2;
		sizeDefault=20;
		nodesList = new LinkedList<Node>();
		edgesList = new LinkedList<Edge>();
		elementsCopied=elementsCopier_;
		undoManager=new UndoManager();
		selection = new Selection(this);

//		for (int i = 0; i < 50000; i++) 
//		{
//			ajoutSommet(0,0);
//		}
	}

	/**
	 * Ajoute une arête en fonction d'un sommet source et d'un sommet
	 * destination, de la couleur actuelle du graphe et de l'épaisseur du trait
	 * et de la taille par défaut
	 * 
	 * @param sour
	 *            Le sommet source
	 * @param dest
	 *            Le sommet destination
	 */
	public void addEdge(Node sour, Node dest) {

		if(edgesList.add(new Edge(defaultColor, thiknessDefaut, sour, dest)))
		{

			//Ajout a l'undo redo
			URaddEdge uRaddEdge=new URaddEdge(edgesList.getLast());
			uRaddEdge.setSommetsEtAretes(nodesList, edgesList);

			getUndoManager().undoableEditHappened(new UndoableEditEvent(this, uRaddEdge));

			setChanged();
			notifyObservers();
		}
	}

	/**
	 * Ajoute un sommet en fonction de x et y,de la couleur actuelle du graphe
	 * de l'épaisseur du trait , de la taille et du type de la forme
	 * (rectangle rond ou carré )
	 * 
	 * @param x
	 *            La position du futur sommet en x
	 * @param y
	 *            La position du futur sommet en y
	 */
	public void addNode(int x, int y) {

		if(nodesList.add(new Node(defaultColor, thiknessDefaut, x , y, getSizeDefault(), getTypeShape())))
		{

			//Ajout a l'undo redo
			URaddNode uRaddNode=new URaddNode(nodesList.getLast());
			uRaddNode.setSommetsEtAretes(nodesList, edgesList);

			getUndoManager().undoableEditHappened(new UndoableEditEvent(this, uRaddNode));

			setChanged();
			notifyObservers();
		}
	}

	/**
	 * Retourne si une arete existe déja entre node1 et node2
	 * 
	 * @return si une arete existe déja
	 */
	public boolean existingEdge(Node node1, Node node2) {
		for (Edge arete : edgesList) 
		{
			if ((arete.getSource().equals(node1) && arete.getTarget().equals(
					node2))
					|| (arete.getSource().equals(node2) && arete.getTarget()
							.equals(nodeTmp)))
				return false;
		}
		return true;
	}

	/**
	 * Colle les elements copiés sur le graph
	 * 
	 * @return si une arete existe déja
	 */
	public void past() {
		if(pointRight!=null)
		{
			selection.past(pointRight.x, pointRight.y);
			setChanged();
			notifyObservers();
		}

	}

	/**
	 * Copie les elements sélectionnés sur le graph
	 * 
	 * @return si une arete existe déja
	 */
	public void copy() {
		selection.copy();
		resetSelection();
	}

	/**
	 * Copie et supprime les elements sélectionnés sur le graph
	 * 
	 * @return si une arete existe déja
	 */
	public void cut() {
		selection.copy();
		deleteSelectedElements();
		
		setChanged();
		notifyObservers();
	}

	/**
	 * Execute l' algorithme de coloration (en fonction du nombre chromatique)
	 * 
	 * @return si une arete existe déja
	 */
	public void executeChromaticAlgorithm() {
		ChromaticAlgorithm chrom=new ChromaticAlgorithm(this);
		chrom.executeAlgorithm();
		setChanged();
		notifyObservers();
	}


	/**
	 * Retourne la liste des arêtes
	 * 
	 * @return la liste des arêtes
	 */
	public LinkedList<Edge> getEdges() {
		return edgesList;
	}
	
	
	/**
	 * Retourne la couleur des futurs sommets
	 * 
	 * @return la couleur des futurs sommets
	 */
	public Color getColorDefault() {
		return defaultColor;
	}
	

	/**
	 * Retourne la liste des aretes associées a un sommet
	 * 
	 * @return  la liste des aretes associées a un sommet
	 */
	public LinkedList<Edge> getEdgesOfNode(Node n) 
	{
		LinkedList<Edge> edgeOfNodes=new LinkedList<Edge>();
		for (Edge edge : edgesList) {
			if (edge.getSource().equals(n) || edge.getTarget().equals(n)) {
				edgeOfNodes.add(edge);
			}
		}
		return edgeOfNodes;
	}
	
	/**
	 * Retourne la liste des élements copiés
	 * 
	 * @return   la liste des élements copiés
	 */
	public CopiedElements getCopiedElement() {
		return elementsCopied;
	}
	
	/**
	 * Retourne l'épaisseur des futurs sommets
	 * 
	 * @return l'épaisseur des futurs sommets
	 */
	public int getThicknessDefault() {
		return thiknessDefaut;
	}
		
	/**
	 * Retourne la selection
	 * 
	 * @return  la selection
	 */
	public Selection getSelection() {
		return selection;
	}
	
	/**
	 * Retourne la liste des sommets
	 * 
	 * @return la liste des sommets
	 */
	public LinkedList<Node> getNodes() {
		return nodesList;
	}

	
	/**
	 * Retourne le sommet temporaire
	 * 
	 * @return le sommet temporaire
	 */
	public Node getNodeTmp() {
		return nodeTmp;
	}

	/**
	 * Retourne la taille des futurs sommets
	 * 
	 * @return la taille des futurs sommets
	 */
	public int getSizeDefault() {
		return sizeDefault;
	}


	/**
	 * Retourne la forme des futurs sommets
	 * 
	 * @return la forme des futurs sommets
	 */
	public String getTypeShape() {
		return typeShape;
	}

	/**
	 * Retourne si on est en mode déplacement
	 * 
	 * @return si on est en mode déplacement
	 */
	public boolean isInMoveMode() {
		return moveMode;
	}

	/**
	 * Enlève les sommets et les arêtes de la selection
	 */
	public void resetSelection() {
		selection.resetSelection();
		setChanged();
		notifyObservers();
	}

	/**
	 * Selectionne les sommets et les aretes du graph si ils sont dans le
	 * rectangle rectTmp passé en parametre
	 */
	public void selectNodesOrEdges(Rectangle rectTmp, boolean ctrl) 
	{
		LinkedList<Node> nodesTmpAdd=new LinkedList<Node>();
		LinkedList<Edge> edgesTmpAdd=new LinkedList<Edge>();

		LinkedList<Node> nodesTmpRemove=new LinkedList<Node>();
		LinkedList<Edge> edgesTmpRemove=new LinkedList<Edge>();

		/* On parcours la liste de sommet */
		for (Node s : this.getNodes()) {
			/*
			 * Si un sommet touche le rectangle de selection alors il devient
			 * sélectionné et on l'ajoute a la liste de sommet a ajouter a la selection..
			 */
			if (s.inRectangle(rectTmp.x, rectTmp.y, rectTmp.width,
					rectTmp.height)) 
			{
					s.setSelected(true);
					nodesTmpAdd.add(s);
			} 
			else 
			{
				/*
					Sinon on le deselectionne sauf si ctrl est maintenu
				 */
				if (!ctrl) 
				{
					s.setSelected(false);
					nodesTmpRemove.add(s);
				}
			}
		}
		/*
		Meme traitement pour les aretes
		 */
		for (Edge e : edgesList) 
		{
			if (e.inRectangle(rectTmp.x, rectTmp.y, rectTmp.width,rectTmp.height)) 
			{
				e.setSelected(true);
				edgesTmpAdd.add(e);

			} 
			else 
			{
				if (!ctrl) 
				{
					e.setSelected(false);
					edgesTmpRemove.add(e);

				}
			}
		}

		//on supprime les elements à enlever de la selection
		selection.removeEdgesFromSelection(edgesTmpRemove);
		selection.removeNodesFromSelection(nodesTmpRemove);

		//on ajoute les elements à la selection
		selection.addeEdgesFromSelection(edgesTmpAdd);
		selection.addNodesToSelection(nodesTmpAdd);

		setChanged();
		notifyObservers();
	}

	/**
	 * Selectionne un sommet ou une arete si ils 
	 * contiennent le noeud passé en paramêtre
	 */
	public void selectEdgeOrNode(Point pt) 
	{
		Node tmp=nodeClicked(pt);
		//Si on a clique sur un sommet alors il devient selectionné
		// si il l'est deja on le deselectionne
		if(tmp!=null)
		{
			tmp.setSelected(!tmp.isSelected());

			if (tmp.isSelected()) 
			{
				selection.addNodeToSelection(tmp);
			} else 
			{
				selection.removeNodeFromSelection(tmp);
			}
		}
		//Si aucun sommet n'a ete touche on autorise la selection d'une arete
		// si elle contient le point de la souris
		//
		else
		{
			for (Edge edge : edgesList) 
			{
				if(edge.containsPoint(pt))
				{
					edge.setSelected(!edge.isSelected());

					if (edge.isSelected()) 
					{
						selection.addEdgeToSelection(edge);
					} else 
					{
						selection.removeEdgeFromSelection(edge);
					}
				}
			}
		}
		setChanged();
		notifyObservers();
	}

	/**
	 * Affecte une nouvelle couleur pour les futurs sommets et arétes du graphe
	 * 
	 * @param defaultColor
	 *            La nouvelle couleur
	 */
	public void setDefaultColor(Color defaultColor) {
		this.defaultColor = defaultColor;
	}

	/**
	 * Affecte une nouvelle couleur à tous les élément sélectionnés
	 * 
	 * @param c
	 *         La nouvelle couleur
	 */
	public void setColorToSelection(Color c) 
	{
		boolean nodeOrEdgeSelected=( selection.getEdgesSelected().size() > 0 || selection.getNodesSelected().size() >0) ;
		for (Edge edge : selection.getEdgesSelected()) {
			edge.setColor(c);

		}
		for (Node node : selection.getNodesSelected()) {
			node.setColor(c);
		}
		if(nodeOrEdgeSelected)
		{
			setChanged();
			notifyObservers();
		}
	}

	

	/**
	 * Affecte une nouvelle epaisseur qui sera effective pour tous les nouveaux elements
	 * 
	 * @param value
	 *            La nouvelle valeur
	 */
	public void setThicknessDefault(int epaisseurDefault_) {
		this.thiknessDefaut = epaisseurDefault_;
	}

	/**
	 * Affecte une nouvelle epaisseur à tous les éléments sélectionnés
	 * 
	 * @param value
	 *            La nouvelle valeur
	 */
	public void setThicknessToSelection(int value) 
	{
		boolean nodeOrEdgeSelected=( selection.getEdgesSelected().size() > 0 || selection.getNodesSelected().size() >0) ;
		
		for (Edge edge : selection.getEdgesSelected()) {
			edge.setThickness(value);
		}
		for (Node node : selection.getNodesSelected()) {
			node.setThickness(value);
		}
		if(nodeOrEdgeSelected)
		{
			setChanged();
			notifyObservers();
		}
	}

	/**
	 * Change le mode (deplacement ou création)
	 * 
	 * @param modeDeplacement
	 *            Le nouveau mode
	 */
	public void setMoveMode(boolean modeDeplacement) 
	{
		this.moveMode = modeDeplacement;
	}

	/**
	 * Affecte un nouveau nom à l'élement sélectionné
	 * 
	 * @param text
	 *            Le nouveau nom
	 */
	public void setNameSelectedElement(String text) 
	{
		if(!text.isEmpty() && text.trim().length() > 0)
		{
			for (Edge edge : selection.getEdgesSelected()) {
				edge.setName(text.trim());

			}
			for (Node node : selection.getNodesSelected()) {
				node.setName(text.trim());
			}
		}
		setChanged();
		notifyObservers();
	}
	/**
	 * Met une reference sur un sommet à nodeTmp (qui peut être nulle)
	 * 
	 * @param text
	 *            Le nouveau nom
	 */
	public void setNodeTmp(Node nodeTmp_) 
	{
		this.nodeTmp = nodeTmp_;
		setChanged();
		notifyObservers();
	}

	/**
	 * Affecte une nouvelle taille pour les futurs sommets
	 * 
	 * @param sizeDefault
	 *            La nouvelle taille
	 */
	public void setSizeDefault(int tailleDefaut_) {
		this.sizeDefault = tailleDefaut_;

		boolean nodeSelected=(selection.getNodesSelected().size()>0);

		for (Node node : selection.getNodesSelected()) {
			node.setSize(sizeDefault);
		}
		if(nodeSelected)
		{
			setChanged();
			notifyObservers();
		}
	}

	/**
	 * Affecte une nouvelle taille pour tous les sommets selectionnés
	 * 
	 * @param sizeDefault
	 *            La nouvelle taille
	 */
	public void setSizeSelection(int value) 
	{
		boolean nodeSelected=( selection.getEdgesSelected().size() > 0 || selection.getNodesSelected().size() >0) ;

		for (Node node : selection.getNodesSelected()) {
			node.setSize(value);
		}
		if(nodeSelected)
		{
			setChanged();
			notifyObservers();
		}
	}
	/**
	 * Affecte le nouveau type de sommet pour les futurs sommets
	 * 
	 * @param typeShape
	 *            Le nouveau type de sommet
	 */
	public void setTypeShapeToSelection(String typeSommet_) {
		

		for (Node n : selection.getNodesSelected()) {
			n.setShape(typeSommet_);
		}
		setChanged();
		notifyObservers();
	}
	//return le noeud sur lequel on a cliqué
	public Node nodeClicked(Point point) 
	{
		pointRight = point;
		for (Node s : this.getNodes()) {
			if (s.containMouse(point.x, point.y)) {
				return s;
			}
		}
		return null;
	}
	
	//si un sommet est sous le clic droit on le selectionne
	public void nodeRightClicked(Point point) {

		if (!selection.isElementInSelection()) 
		{
			Node tmp = nodeClicked(point);
			if (tmp != null) 
			{
				if(!tmp.isSelected())
				{
					tmp.setSelected(true);
					selection.addNodeToSelection(tmp);
				}
			}
		}
		nodeTmp=null;
		setChanged();
		notifyObservers();
	}
	/**
	 * Supprime tous les elements sélectionnés
	 * si on supprime un sommet on supprime les aretes qui lui sont associées
	 **/
	public void deleteSelectedElements() 
	{
		if(selection.isElementInSelection() )
		{
			//ajout a l'undo redo
			URremoveElements uRremoveElements=new URremoveElements(selection.getNodesSelected(),selection.getEdgesSelected());
			uRremoveElements.setNodesAndEdgeOfGraph(nodesList, edgesList);

			getUndoManager().undoableEditHappened(new UndoableEditEvent(this, uRremoveElements));
		}
		//Supprime tous les noeuds sélectionnées du graphe et les 
		//aretes qui lui sont associées
		for (Node s : selection.getNodesSelected()) {
			if (s.isSelected() == true) 
			{
				this.getEdges().removeAll(getEdgesOfNode(s)); 
				nodesList.remove(s);
			}
		}
		selection.getNodesSelected().clear();
		
		
		//Supprime tous les aretes sélectionnées du graphes
		for (Edge a : selection.getEdgesSelected()) {
			if (a.isSelected() == true) {
				this.getEdges().remove(a);
			}
		}
		selection.getEdgesSelected().clear();
		nodeTmp=null;
		setChanged();
		notifyObservers();
	}

	/**
	 * Annule la derniere action effectuée
	 **/
	public void undo() 
	{
		if(this.getUndoManager().canUndo())
			this.getUndoManager().undo();
		
		this.resetSelection();
		this.nodeTmp=null;
		setChanged();
		notifyObservers();
	}

	/**
	 * Rexecute la derniere action annulée
	 **/
	public void redo() {
		if(this.getUndoManager().canRedo())
			this.getUndoManager().redo();

		this.resetSelection();
		this.nodeTmp=null;
		setChanged();
		notifyObservers();
	}

	/**
	 * Déplace tous les elements selectionnés
	 **/
	public void moveSelectedNode(HashMap<Node, Point> mapOfNodeSelected) 
	{
		//ajout a l'undo redo
		URmoveNodes uRmoveNodes=new URmoveNodes(mapOfNodeSelected);
		
		for(Entry<Node, Point> entry : mapOfNodeSelected.entrySet()) 
		{
		    Node node = entry.getKey();
		    Point newPosition = entry.getValue();
		    Point lastPos=new Point(node.getX(),node.getY());
		    node.move(newPosition.x,newPosition.y);
		    entry.getValue().x=lastPos.x;
		    entry.getValue().y=lastPos.y;
		}
		
		getUndoManager().undoableEditHappened(new UndoableEditEvent(this, uRmoveNodes));
		setChanged();
		notifyObservers();
	}

	/**
	 * Execute l'algorithm de répartition circulaire du graphe
	 **/
	public void executeCircularLayoutAlgorithm() {
		CircularLayout c=new CircularLayout(this);
		c.executeAlgorithm();
		setChanged();
		notifyObservers();
	}

	/**
	 * Importe un graph a partir d'un fichier graphml
	 **/
	public void importGraphml(String fileToImport) {
		GraphmlExportImport impo=new GraphmlExportImport(this);
		impo.importGraphml(fileToImport);
		setChanged();
		notifyObservers();
		
	}

	/**
	 * Exporte le graphe en graphml a l'adresse fileToExport
	 **/
	public void exportGraphml(String fileToExport) {
		GraphmlExportImport impo=new GraphmlExportImport(this);
		impo.exportGraphml(fileToExport);
	}

	/**
	 * Change le type de forme pour les futurs sommets
	 **/
	public void setDefaultTypeShape(String selectedItem) {
		this.typeShape = selectedItem;
	}

	/**
	 * Ajoute une propriété a l'élément sélectionnée 
	 **/
	public void addPropertyToElement(String key, Double value) {
		selection.addPropertyToNode(key, value);
	}

	
	/**
	 * Retourne la liste de tous les propriétés cumulées du graph
	 **/
	public HashSet<String> getAllProperties() 
	{
		HashSet<String> listProperty  =  new HashSet<String>() ;

		for (Node n : nodesList) 
		{
			for(Entry<String, Double> entry : n.getNodeAttributes().entrySet())
			{
				listProperty.add(entry.getKey());
			}
		}
		return listProperty;
	}

	/**
	 * Execute l'algoritmh de calcul de taille en fonction d'une propriété
	 **/
	public void executeAlgoTaille(String selectedItem, int valueMin, int valueMax) 
	{
		SizeAlgorithm sizeAlgorithm =new SizeAlgorithm(this,selectedItem,valueMin,valueMax);
		sizeAlgorithm.executeAlgorithm();
		setChanged();
		notifyObservers();
	}

	/**
	 * Execute l'algoritmh de calcul des couleurs en fonction d'une propriété (même traitement que executeAlgoTaille)
	 **/
	public void executeAlgorithmColor(String selectedItem, Color background,
			Color background2) 
	{
		ColorAlgorithm colorAlgorithm =new ColorAlgorithm(this,selectedItem,background,background2);
		colorAlgorithm.executeAlgorithm();
		setChanged();
		notifyObservers();
	}

	public UndoManager getUndoManager() {
		return undoManager;
	}
	
	/**
	 * Execute l'algoritmh de calcul des degrée de chaque sommet
	 **/
	public void executeAlgorithmDegree() {
		DegreeAlgorithm degreeAlgorithm =new DegreeAlgorithm(this);
		degreeAlgorithm.executeAlgorithm();
		setChanged();
		notifyObservers();
	}

	/**
	 * Exporte le graph en .dot
	 **/
	public void exportDot(String fileToExport) {
		DotExportImport impo=new DotExportImport(this);
		impo.exportDot(fileToExport);
	}

	/**
	 * Import le graph a partir .dot
	 **/
	public void importDot(String fileToImport) {
		this.nodesList.clear();
		this.edgesList.clear();
		DotExportImport impo=new DotExportImport(this);
		impo.importDot(fileToImport);
		setChanged();
		notifyObservers();
	}

	/**
	 * Supprime une propriété de l'element selectionné
	 **/
	public void removePropertyOfElement(String o) {
		selection.removePropertyOfElement(o);
	}

	/**
	 * Modifie l'étiquette d'un élément et lui affecte une de ses propriétés
	 **/
	public void changeNameOfElement(String o) {
		selection.changeNameOfElement(o);
		setChanged();
		notifyObservers();
	}

	/**
	 * Execute l'algoritmh de calcul des degrée de chaque sommet
	 **/
	public void executeAlgorithmRandom(Point[] points) {
		RandomAlgorithm randomAlgorithm =new RandomAlgorithm(this,points);
		randomAlgorithm.executeAlgorithm();
		setChanged();
		notifyObservers();
	}

}
