package Model;

import java.awt.Color;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map.Entry;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * <p>
 *  Sommet est la classe représentant une forme soit :
 * <ul>
 * 		<li>Un rectangle</li> 
 * 		<li>Un rond</li>
 * 		<li>Un triangle</li>
 * </ul>
 * </p>
 * <p>
 * Un sommet est caractérisé par les informations suivantes :
 * <ul>
 * 		<li>Une couleur</li> 
 * 		<li>Une épaisseur</li>
 * 		<li>Un nom (ou étiquette )</li>
 * 		<li>Un id</li>
 * </ul>
 * La taille, la forme et la position du sommet sont contenus
 * dans l'attribut forme.
 * </p>
 * 
 * @see Rectangle
 * @see Circle
 * @see Triangle
 * @see Shape
 * 
 * @author Manu Patrois
 * @version 1.0
 * 
 */
public class Node 
{
	/**
	 * La forme du sommet.<br>
	 * Elle n'est pas définitive
	 */
	private Shape shape=null;


	/**
	 * Le nombre de sommet.<br>
	 * Permet d'affecter un nouvel id chaque nouveau sommet 
	 */
	private static int nbNode=0;


	/**
	 * L'identifiant du sommet.<br>
	 * Deux sommets ne peuvent avoir le même id
	 */
	private int idNode;


	/**
	 * La couleur du sommet.<br>
	 * Elle n'est pas définitive
	 */
	private Color color;

	/**
	 * L'épaisseur des traits du sommet
	 * Elle n'est pas définitive
	 */
	private int thickness;

	/**
	 * Le nom ou étiquette du sommet.<br>
	 * Il n'est pas définitif
	 */
	private String name;

	/**
	 * Si le sommet a été selectionné <br>
	 * 
	 */
	private boolean selected=false;
	
	private HashMap<String,Double> nodeAttributes=new HashMap<>();

	/**
	 * Instancie un nouveau sommet.
	 * 
	 * @param col
	 *            La couleur du sommet
	 * @param thick
	 * 			  L'épaisseur
	 * @param x
	 * 			  L'abscisse du sommet
	 * @param y
	 * 			  L'ordonn�e du sommet
	 * @param size
	 * 			  La taille du sommet
	 * 
	 * @param typeS
	 * 			  La forme du sommet
	 */
	public Node(Color col,int thick,int x,int y,int size,String typeS)
	{
		nbNode++;
		idNode=nbNode;
		color=col;
		thickness=thick;

		if(typeS=="Rectangle")
			setShape(new Rectangle(x,y,size));
		else if(typeS=="Rond")
			setShape(new Circle(x,y,size));
		else if(typeS=="Triangle")
			setShape(new Triangle(x,y,size));
		
		addAttributes("id",idNode);
		name="id("+idNode+")";

	};
	
	
	public void addAttributes(String key,double value)
	{
		getNodeAttributes().put(key, value);
	}
	
	
	
	/**
	 * Instancie un nouveau sommet par copie d'un autre
	 * 
	 * @param s
	 * 			  Le sommet copié
	 */
	public Node(Node s)
	{
		nbNode++;
		idNode=nbNode;
		color=s.color;
		thickness=s.thickness;
		name=s.getName();
		selected=false;

		if(s.getTypeShape()=="Rectangle")
			setShape(new Rectangle(s.getX(),s.getY(),s.getSize()));
		else if(s.getTypeShape()=="Rond")
			setShape(new Circle(s.getX(),s.getY(),s.getSize()));
		else if(s.getTypeShape()=="Triangle")
			setShape(new Triangle((Triangle)s.getShape()));
		
		
		for(Entry<String, Double> entry : s.nodeAttributes.entrySet()) 
		{
			this.addAttributes(entry.getKey(),entry.getValue());
		}
	};
	
	public Node(Element node)
	{
		
		idNode=Integer.parseInt(node.getAttribute("id"));
		if(nbNode<idNode)
			nbNode=idNode+1;
		
		String type="";
		int width=0;
		int x=0;
		int y=0;
		NodeList data=node.getElementsByTagName("data");
		for(int j = 0; j<data.getLength(); j++) 
		{
			final Element d = (Element) data.item(j);
			String key=d.getAttribute("key");
			String content=d.getTextContent();
			if(key.equals("d0"))
			{
				 int red=Integer.parseInt(content.split(";")[0]);
				 int green=Integer.parseInt(content.split(";")[1]);
				 int blue=Integer.parseInt(content.split(";")[2]);
				 color=new Color(red,green,blue);
			}
			else if(key.equals("d1"))
			{
				name = content;
			}
			else if(key.equals("d2"))
			{
				type = content;
			}
			else if(key.equals("d3"))
			{
				width = Integer.parseInt(content);
			}
			else if(key.equals("d4"))
			{
				thickness = Integer.parseInt(content);
			}
			else if(key.equals("d5"))
			{
				x = Integer.parseInt(content);
			}
			else if(key.equals("d6"))
			{
				y = Integer.parseInt(content);
			}
			
		}
		if(type.equals("Rectangle"))
			setShape(new Rectangle(x,y,width));
		else if(type.equals("Rond"))
			setShape(new Circle(x,y,width));
		else if(type.equals("Triangle"))
			setShape(new Triangle(x,y,width));

		selected=false;

	};

	public Node(String string) 
	{
		nbNode++;
		String id=string.split("\\[")[0];
		String attributs=string.split("\\[")[1].split("\\]")[0];
		String defaultAttributes=attributs.split(";,")[0].trim();
		String otherAttributes=attributs.split(";,")[1].trim();
		
		idNode=Integer.parseInt(id.trim());
		
		//----Traitement propriété defaut------------------------------------;
		String label=defaultAttributes.split("label=")[1].split(",")[0];
		String x_=defaultAttributes.split("x=")[1].split(",")[0];
		String y_=defaultAttributes.split("y=")[1].split(",")[0];
		String thickness_=defaultAttributes.split("thickness=")[1].split(",")[0];
		String size_=defaultAttributes.split("size=")[1].split(",")[0];
		String color_=defaultAttributes.split("color=")[1];
		String shape_=defaultAttributes.split("shape=")[1].split(",")[0];
		this.name=label;
		this.thickness=Integer.parseInt(thickness_);
		int red=Integer.parseInt(color_.split(";")[0]);
		int green=Integer.parseInt(color_.split(";")[1]);
		int blue=Integer.parseInt(color_.split(";")[2]);
		this.color=new Color(red,green,blue);
		int size=Integer.parseInt(size_);
		int x=Integer.parseInt(x_);
		int y=Integer.parseInt(y_);
		if(shape_.equals("Rectangle"))
			setShape(new Rectangle(x,y,size));
		else if(shape_.equals("Rond"))
			setShape(new Circle(x,y,size));
		else if(shape_.equals("Triangle"))
			setShape(new Triangle(x,y,size));
		
		//----Traitement propriété autre-----------------------3");
		for (String s: otherAttributes.split(",")) 
		{
			String key=s.trim().split("=")[0].trim();
			String value=s.trim().split("=")[1].trim();
			double val=Double.parseDouble(value);
			this.addAttributes(key, val);
		}
	}


	/**
	 * Déplace le sommet en fonction de x et y <br>
	 * @param x
	 * Déplace posX du sommet (de sa forme) en x
	 * @param y
	 * Déplace posY du sommet (de sa forme) en y
	 */
	public void move(int x,int y)
	{
		getShape().move(x, y);
	}
	
	
//	public boolean isSameNode(Node somTest)
//	{
//		if( getX()==somTest.getX()
//			&& getY()==somTest.getY()
//			&& getSize()==somTest.getSize()
//			&& getColor()==somTest.getColor()
//			&& getThickness()==somTest.getThickness()
//			&& getName().equals(somTest.getName())
//				)
//			return true;
//		else
//			return false;
//	}
	/**
	 * Renvoi si les coordonnées coordX et coordY d'un point sont 
	 * contenues dans la forme du sommet.
	 * @param coordX
	 * 		Abscisse de la souris
	 * @param zoom
	 * 		Ordonn�e de la souris
	 * @return boolean
	 */
	public boolean containMouse(int coordX,int coordY)
	{
		return getShape().containMouse(coordX, coordY);
	}
	;

	/**
	 * Retourne le centre du sommet sous forme de point
	 * @return boolean
	 */
	public Point centerNode(){
		return getShape().getCenterShape();
	}

	/**
	 * Retourne la couleur du sommet
	 * @return Couleur du sommet
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Affecte une nouvelle couleur au sommet
	 * @param couleur
	 * 			La nouvelle couleur
	 */
	public void setColor(Color couleur) {
		this.color = couleur;
	}

	/**
	 * Retourne l'�paisseur du sommet
	 * @return L'�paisseur du sommet 
	 */
	public int getThickness() {
		return thickness;
	}

	/**
	 * Affecte une nouvelle epaisseur au sommet
	 * @param epaisseur
	 * 			La nouvelle epaisseur
	 */
	public void setThickness(int epaisseur) {
		this.thickness = epaisseur;
	}

	/**
	 * Retourne le nom du sommet
	 * @return Le nom du sommet
	 */
	public String getName() {
		return name;
	}

	/**
	 * Affecte un nouveau nom au sommet
	 * @param thickness
	 * 			La nouvelle epaisseur
	 */
	public void setName(String nom) {
		this.name = nom;
	}

	/**
	 * Retourne la position en abscisse du sommet
	 * @return int
	 */
	public int getX() {
		return getShape().getX();
	}


	/**
	 * Affecte une nouvelle position en abscisse
	 * @param posX
	 * 			La nouvelle position en abscisse
	 */
	public void setX(int posX) {
		getShape().setX(posX);
	}

	/**
	 * Retourne la position en ordonn�e du sommet
	 * @return La position en ordonn�e
	 */
	public int getY() {
		return getShape().getY();
	}

	/**
	 * Affecte une nouvelle position en ordonn�e
	 * @param posY
	 * 			La nouvelle position en ordonn�e
	 */
	public void setY(int posY) 
	{
		getShape().setY(posY);
	}

	/**
	 * Retourne la taille du sommet
	 * @return La taille du sommet
	 */
	public int getSize() {
		return getShape().getSize();
	}

	/**
	 * Affecte une nouvelle taille au sommet
	 * @param taille
	 * 			La taille du sommet
	 */
	public void setSize(int w) 
	{
		getShape().setWidth(w);
	}

	/**
	 * Definit si le sommet est selectionn� ou pas
	 * @param b
	 * 			Si le sommet est selectionn� ou non
	 */
	public void setSelected(boolean b) {
		this.selected=b;

	}

	/**
	 * Défini si le sommet a été selectionné
	 * @param b
	 * 			Si le sommet a été sélectionné ou non
	 */

	public boolean isSelected() {
		return selected;
	}

	

	/**
	 * Retourne si le sommet touche le rectangle pass� en
	 * paramètre
	 * @return Si le sommet touche un rectangle 
	 */
	public boolean inRectangle(int x,int y,int w,int h) 
	{
		return getShape().inRectangle(x,y,w,h);
	}

	/**
	 * Retourne si le sommet touche le rectangle pass� en
	 * paramètre
	 * @return Si le sommet touche un rectangle 
	 */
	public String getTypeShape()
	{
		return getShape().getTypeShape();
	};

	/**
	 * Affecte une nouvelle forme au sommet
	 */
	public void setShape(String typeS)
	{
		int x=shape.getX();
		int y=shape.getY();
		int w=shape.getSize();

		shape=null;
		if(typeS=="Rectangle")
			shape=new Rectangle(x,y,w);
		else if(typeS=="Rond")
			shape=new Circle(x,y,w);
		else if(typeS=="Triangle")
			shape=new Triangle(x,y,w);
	}
	
	
	/**
	 * @return  Retourne l'id du sommet
	 */
	public int getIdNode() {
		return idNode;
	}


	/**
	 * @return  Retourne la forme du sommet
	 */
	public Shape getShape() {
		return shape;
	}

	/**
	 * Affecte un nouvelle forme au sommet
	 */
	public void setShape(Shape shape) {
		this.shape = shape;
	}
	
	
	/**
	 * Retourne le sommet sous forme graphml
	 */
	public Element exportNodeXml(Document doc)
	{
		Element node=doc.createElement("node");
		node.setAttribute("id", idNode+"");
		
		Element Color=doc.createElement("data");
		Color.setAttribute("key", "d0");
		Color.setTextContent(color.getRed()+";"+color.getGreen()+";"+color.getBlue());
		node.appendChild(Color);
		
		Element Name=doc.createElement("data");
		Name.setAttribute("key", "d1");
		Name.setTextContent(name);
		node.appendChild(Name);
		
		Element TypeShape=doc.createElement("data");
		TypeShape.setAttribute("key", "d2");
		TypeShape.setTextContent(getShape().getTypeShape());
		node.appendChild(TypeShape);
		
		Element Width=doc.createElement("data");
		Width.setAttribute("key", "d3");
		Width.setTextContent(getShape().getSize()+"");
		node.appendChild(Width);
		
		Element Thickness=doc.createElement("data");
		Thickness.setAttribute("key", "d4");
		Thickness.setTextContent(thickness+"");
		node.appendChild(Thickness);
		
		Element X=doc.createElement("data");
		X.setAttribute("key", "d5");
		X.setTextContent(getShape().getX()+"");
		node.appendChild(X);
		
		Element Y=doc.createElement("data");
		Y.setAttribute("key", "d6");
		Y.setTextContent(getShape().getY()+"");
		node.appendChild(Y);
		
		return node;
	
	}
	
	/**
	 * Retourne la definiton des attributs d'un sommet en graphml
	 */
	public static void addAttributesGraphml(Document doc,Element root)
	{
		Element keyColor=doc.createElement("key");
		keyColor.setAttribute("id", "d0");
		keyColor.setAttribute("for", "node");
		keyColor.setAttribute("attr.name", "color");
		keyColor.setAttribute("attr.type", "Color");
		root.appendChild(keyColor);
		
		Element keyName=doc.createElement("key");
		keyName.setAttribute("id", "d1");
		keyName.setAttribute("for", "node");
		keyName.setAttribute("attr.name", "name");
		keyName.setAttribute("attr.type", "String");
		root.appendChild(keyName);
		
		Element keyShape=doc.createElement("key");
		keyShape.setAttribute("id", "d2");
		keyShape.setAttribute("for", "node");
		keyShape.setAttribute("attr.name", "shape");
		keyShape.setAttribute("attr.type", "Shape");
		root.appendChild(keyShape);
		
		Element keyWidth=doc.createElement("key");
		keyWidth.setAttribute("id", "d3");
		keyWidth.setAttribute("for", "node");
		keyWidth.setAttribute("attr.name", "width");
		keyWidth.setAttribute("attr.type", "int");
		root.appendChild(keyWidth);
		
		Element keyThickness=doc.createElement("key");
		keyThickness.setAttribute("id", "d4");
		keyThickness.setAttribute("for", "node");
		keyThickness.setAttribute("attr.name", "thickness");
		keyThickness.setAttribute("attr.type", "int");
		root.appendChild(keyThickness);
		
		Element keyX=doc.createElement("key");
		keyX.setAttribute("id", "d5");
		keyX.setAttribute("for", "node");
		keyX.setAttribute("attr.name", "x");
		keyX.setAttribute("attr.type", "int");
		root.appendChild(keyX);
		
		Element keyY=doc.createElement("key");
		keyY.setAttribute("id", "d6");
		keyY.setAttribute("for", "node");
		keyY.setAttribute("attr.name", "y");
		keyY.setAttribute("attr.type", "int");
		root.appendChild(keyY);

	}
	public String exportNodeDot()
	{
		String nodeDot=""+idNode;
		nodeDot+=" [";
		
		nodeDot+="label="+name+", ";
		nodeDot+="x="+getX()+", ";
		nodeDot+="y="+getY()+", ";
		nodeDot+="thickness="+getThickness()+", ";
		nodeDot+="size="+getSize()+", ";
		nodeDot+="shape="+getTypeShape()+", ";
		nodeDot+="color="+color.getRed()+";"+color.getGreen()+";"+color.getBlue()+";, ";
		
		for(Entry<String, Double> entry : nodeAttributes.entrySet()) 
		{
			nodeDot+=entry.getKey()+"="+entry.getValue()+", ";
		}
		nodeDot+="id="+idNode+"];";
		
		return nodeDot;
	}
	public void removeProperty(String key)
	{
		nodeAttributes.remove(key);
	}


	public void setBounds(int x, int y, int w) {
		shape.setX(x);
		shape.setY(y);
		shape.setWidth(w);
	}

	public HashMap<String,Double> getNodeAttributes() {
		return nodeAttributes;
	}

	void setNodeAttributes(HashMap<String,Double> nodeAttributes) {
		this.nodeAttributes = nodeAttributes;
	}

	
	
}
