package Model;

import java.awt.Point;


/**
*
* <p>
*  Rectangle est une classe héritée de la classe forme
* </p>
* 
* @see Node
* @see Shape
* 
* @author Intelli'Graph
* @version 1.0
* 
*/
public class Rectangle extends Shape {

	/**
	 * Instancie un nouveau Rectangle.
     * @param x
     * 			  L'abscisse du Rectangle
     * @param y
     * 			  L'ordonnée du Rectangle
     * @param tail
     * 			  La taille du Rectangle
     */
	Rectangle(int x, int y, int tail) {
		super(x, y, tail);
	}

	
//	@Override
//	void tracerSommet(Graphics g, float zoom, Point origin) 
//	{
//
//			g.drawRect( (int)(getX()*zoom+origin.x*zoom),
//						(int)(getY()*zoom+origin.y*zoom),
//						(int)(getSize()*zoom),
//						(int)(getSize()*zoom));
//	}

	@Override
	public boolean containMouse(int coordX, int coordY)
	{
		if (coordX>=this.getX() && coordX <= (this.getX()+this.getSize())  &&
				coordY>=this.getY() && coordY <= (this.getY()+this.getSize()) )
			return true;
		else 
			return false;	
	}

	@Override
	public Point getCenterShape()
	{
		return new Point(this.getX()+this.getSize()/2,this.getY()+this.getSize()/2);
	}

	@Override
	public String getTypeShape() {
		return "Rectangle";
	}

	@Override
	boolean inRectangle(int x,int y,int w,int h) 
	{
		return !(x > getX()+getSize() || 
		           x+w < getX() || 
		           y > getY()+getSize() ||
		           y+h <getY());
	}

}

