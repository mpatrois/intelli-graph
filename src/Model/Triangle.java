package Model;

import java.awt.Point;
import java.awt.Polygon;


/**
La classe triangle hérite de la classe forme.
 <p>
 * Le triangle est caractérisé par 3 points.
 * Le dessin du triangle est le tracé de lignes 
 * entre ses trois points.
 * 
 * @see Node
 * 
 * @author Intelli'Graph Team
 * @version 1.0
 */
public class Triangle extends Shape 
{

	private Point droite;
	private Point gauche;
	
	/**
	 * Instancie un nouveau Triangle.
     * @param x
     * 			  L'abscisse du Triangle
     * @param y
     * 			  L'ordonnée du Triangle
     * @param tail
     * 			  La taille du Triangle
     */
	Triangle(int x, int y, int tail) 
	{
		
		super(x,y,tail);
		/* Créer trois point qui seront reliés ensemble lors du dessin et utilisé lors des calculs 
		 containsMouse, inRectangle ,move etc..*/
		//setHaut(new Point(x,y-tail/2));
		droite=new Point(posX+getSize(),posY+getSize()/2+getSize());
		gauche=new Point(posX-getSize(),posY+getSize()/2+getSize());
	}
	Triangle(Triangle T) 
	{
		
		super(T.posX,T.posY-30,T.getSize());
		
		/* Créer trois point qui seront reliés ensemble lors du dessin et utilisé lors des calculs 
		 containsMouse, inRectangle ,move etc..*/
		//setHaut(new Point(x,y-tail/2));
		droite=T.droite;
		gauche=T.gauche;
	}
	@Override
	public boolean containMouse(int coordX, int coordY) {
		Polygon p=new Polygon();
		p.addPoint(posX,posY);
		p.addPoint(droite.x,droite.y);
		p.addPoint(gauche.x,gauche.y);
		
		return p.contains(coordX,coordY);
	}

	@Override
	public Point getCenterShape() {
		int x=(posX+getGauche().x+getDroite().x)/3;
		int y=(posY+getGauche().y+getDroite().y)/3;
		return new Point(x,y);
	}
	
	@Override
	public void move(int x,int y) 
	{

		posX=x;
		posY=y;
//		droite=new Point(posX+getSize(),posY+getSize());
//		gauche=new Point(posX-getSize(),posY+getSize());
		
		droite=new Point(posX+size,posY+size/2+size);
		gauche=new Point(posX-size,posY+size/2+size);
	}
	
	@Override
	public void moveWithDxAndDy(int dx,int dy) 
	{
		super.moveWithDxAndDy(dx, dy);
		//setHaut(new Point(posX,posY-getSize()/2));
		droite=new Point(droite.x+dx,droite.y+dy);
		gauche=new Point(gauche.x+dx,gauche.y+dy);
	}

	@Override
	public String getTypeShape() {
		return "Triangle";
	}

	@Override
	boolean inRectangle(int x, int y, int w, int h) {
		Polygon p=new Polygon();
		p.addPoint(posX,posY);
		p.addPoint(droite.x,droite.y);
		p.addPoint(gauche.x,gauche.y);
		
		return p.intersects(x, y, w, h);
		
	}

	public Point getDroite() {
		return droite;
	}

	public Point getGauche() {
		return gauche;
	}
	@Override
	void setWidth(int taille) {
		
		size=taille;
		/* Créer trois point qui seront reliés ensemble lors du dessin et utilisé lors des calculs 
		 containsMouse, inRectangle ,move etc..*/
		//setHaut(new Point(x,y-tail/2));
		droite=new Point(posX+getSize(),posY+getSize()/2+getSize());
		gauche=new Point(posX-getSize(),posY+getSize()/2+getSize());
		
		this.size = taille;
	}
}
