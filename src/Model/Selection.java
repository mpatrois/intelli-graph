package Model;

import java.awt.Point;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Observable;

import javax.swing.event.UndoableEditEvent;

import UndoRedo.URaddElements;


//Selection represente les noeuds et aretes selectionnes
//et est ecouté par PropertyView,en effet des qu'un ou plusieurs noeuds/aretes
//sont selectionnés propertyView change (des labels sont en plus etc..) 

public class Selection extends Observable
{

	private LinkedList<Node> sommetSelect=new LinkedList<Node>();
	private LinkedList<Edge> areteSelect=new LinkedList<Edge>();

	GraphModel graph=null;

	Selection(GraphModel g)
	{
		graph=g;
	}

	public boolean isElementInSelection()
	{
		return (getNodesSelected().size()>0 || getEdgesSelected().size()>0);
	}

	public void resetSelection()
	{
		for (Edge a : graph.getEdges()) {
			a.setSelected(false);
		}
		for (Node s : graph.getNodes()) {
			s.setSelected(false);
		}
		getNodesSelected().clear();
		getEdgesSelected().clear();
		
		setChanged();
		notifyObservers();
	}
	
	public void copy()
	{
		graph.getCopiedElement().sommetCopier.clear();
		graph.getCopiedElement().areteCopier.clear();
		for (Edge a : getEdgesSelected())
		{
			graph.getCopiedElement().areteCopier.add(a);
		}
		for (Node s : getNodesSelected())
		{
			graph.getCopiedElement().sommetCopier.add(s);
		}
	}
	public void addNodeToSelection(Node n)
	{
		if(!sommetSelect.contains(n))
		{
			this.sommetSelect.add(n);
			setChanged();
			notifyObservers();
		}
	}
	void addEdgeToSelection(Edge e)
	{
		if(!areteSelect.contains(e))
		{
			this.areteSelect.add(e);
			setChanged();
			notifyObservers();
		}
	}
	void removeNodeFromSelection(Node n)
	{
		this.sommetSelect.remove(n);
		setChanged();
		notifyObservers();
	}
	void removeEdgeFromSelection(Edge e)
	{
		this.areteSelect.remove(e);
		setChanged();
		notifyObservers();
	}
	void past(int x,int y)
	{
		if(!graph.getCopiedElement().sommetCopier.isEmpty())
		{
			Point ptTmp=pointUpLeft();
			HashMap<Node,Node> originalToNewNodes=new HashMap<>();
			LinkedList<Node> nodesToAdd=new LinkedList<Node>();
			for (Node n : graph.getCopiedElement().sommetCopier) 
			{
				Node nodePast=new Node(n);
				nodePast.move(x-(ptTmp.x-nodePast.getX()),y-(ptTmp.y-nodePast.getY()));
				graph.getNodes().add(nodePast);
				nodesToAdd.add(nodePast);
				originalToNewNodes.put(n, nodePast);
			}
			LinkedList<Edge> edgesToAdd=new LinkedList<Edge>();
			for (Edge a : graph.getCopiedElement().areteCopier) 
				{
					if(graph.getCopiedElement().sommetCopier.contains(a.getSource()) && graph.getCopiedElement().sommetCopier.contains(a.getTarget()) )
					{
						Node sour=originalToNewNodes.get(a.getSource());
						Node des=originalToNewNodes.get(a.getTarget());
						Edge tmp=new Edge(a,sour,des);
						graph.getEdges().add(tmp);
						edgesToAdd.add(tmp);
					}
				}
			URaddElements uRraddElements=new URaddElements(nodesToAdd,edgesToAdd);
			uRraddElements.setNodesAndEdgeOfGraph(graph.getNodes(), graph.getEdges());
			
			graph.getUndoManager().undoableEditHappened(new UndoableEditEvent(this, uRraddElements));
		}
		
	}
	Point pointUpLeft()
	{
		int minX=0;
		int minY=0;
		if(graph.getCopiedElement().sommetCopier.size()>0)
		{
			minX=graph.getCopiedElement().sommetCopier.getFirst().getX();
			minY=graph.getCopiedElement().sommetCopier.getFirst().getY();
		}
		for (Node s : graph.getCopiedElement().sommetCopier) 
		{
			if(s.getX()<minX)
				minX=s.getX();
			if(s.getY()<minY)
				minY=s.getY();
		}
		return new Point(minX,minY);
	}
	public LinkedList<Node> getNodesSelected() {
		return sommetSelect;
	}
	public void setSommetSelect(LinkedList<Node> sommetSelect) {
		this.sommetSelect = sommetSelect;
	}
	public LinkedList<Edge> getEdgesSelected() {
		return areteSelect;
	}
	public void setAreteSelect(LinkedList<Edge> areteSelect) {
		this.areteSelect = areteSelect;
	}
	public void removeEdgesFromSelection(LinkedList<Edge> edgesTmpRemove) {
		this.areteSelect.removeAll(edgesTmpRemove);
		setChanged();
		notifyObservers();
	}
	public void removeNodesFromSelection(LinkedList<Node> nodesTmpRemove) 
	{
		this.sommetSelect.removeAll(nodesTmpRemove);
		setChanged();
		notifyObservers();
	}
	public void addeEdgesFromSelection(LinkedList<Edge> edgesTmpAdd) {
		
		this.areteSelect.removeAll(edgesTmpAdd);
		this.areteSelect.addAll(edgesTmpAdd);
		setChanged();
		notifyObservers();
	}

	public void addNodesToSelection(LinkedList<Node> nodesTmpAdd) {
		this.sommetSelect.removeAll(nodesTmpAdd);
		this.sommetSelect.addAll(nodesTmpAdd);
		setChanged();
		notifyObservers();
	}

	public void addPropertyToNode(String key, Double value) 
	{
		if(sommetSelect.size()==1)
		sommetSelect.getFirst().addAttributes(key, value);
		else if(areteSelect.size()==1)
		areteSelect.getFirst().addAttributes(key, value);
			
		setChanged();
		notifyObservers();
	}

	public void removePropertyOfElement(String key) {
		if(sommetSelect.size()==1)
			sommetSelect.getFirst().removeProperty(key);
		else if(areteSelect.size()==1)
			areteSelect.getFirst().removeProperty(key);
				
			setChanged();
			notifyObservers();
	}

	public void changeNameOfElement(String o) 
	{
		if(sommetSelect.size()==1)
			sommetSelect.getFirst().setName(o);
		else if(areteSelect.size()==1)
			areteSelect.getFirst().setName(o);
		setChanged();
		notifyObservers();
	}
}
