package Model;

import java.awt.Color;
import java.awt.Point;
import java.awt.Polygon;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
*
* <p>
Une arête est un trait qui relie deux sommets entre eux
* </p>

* Une arête est caractérisée par les informations suivantes :
 * <ul>
 * 		<li>Une couleur</li> 
 * 		<li>Une épaisseur</li>
 * 		<li>un sommet source</li>
 * 		<li>un sommet destination</li>
 * 
 * </ul>
* 
* @author  Intelli'Graph team
* @version 1.0
* 
*/

public class Edge
{
	/**
	 * La couleur de l'arête
	 */
	private Color color;
	
	/**
	 * L' épaisseur de l'arête
	 */
	private int thickness;
	
	/**
	 * Le nom de l'arête
	 */
	private String name;
	
	/**
	 * Le sommet source de l'arête
	 */
	private Node source=null;
	
	/**
	 * Le sommet destination de l'arête
	 */
	private Node target=null;
	
	/**
	 * La selection de l'arête
	 */
	private boolean selected;
	
	
	private static int nbEdge=0;
	
	
	/**
	 * L'identification de l'arête
	 */
	private int idEdge;
	
	private HashMap<String,Double> edgeAttributes=new HashMap<>();
	
	
	/**
	 * Instancie une nouvelle arete.
	 * 
	 * @param col
	 *            La couleur de l'arete
	 * @param thick
	 * 			  L'épaisseur
	 * @param sour
	 * 			  La source de l'arête
	 * @param targ
	 * 			  La destination de l'arête
	 */

	public Edge(Color col,int thick,Node sour,Node targ)
	{
		nbEdge++;
		idEdge=nbEdge;
		setColor(col);
		setThickness(thick);
		setName("a"+idEdge);
		source=sour;
		target=targ;
		setSelected(false);
	}
	
	/**
	 * Instancie une nouvelle arete par copie d'une autre avec des sommets différents.
	 * 
	 * @param a
	 * 			L'arete dont on va copier les propriétés
	 * @param sour
	 * 			  La source de l'arête
	 * @param targ
	 * 			  La destination de l'arête
	 */
	public Edge(Edge a,Node sour,Node dest)
	{
		nbEdge++;
		idEdge=nbEdge;
		setColor(a.color);
		setThickness(a.thickness);
		setName(a.getName());
		source=sour;
		target=dest;
		setSelected(false);
	}
	public Edge(String string, LinkedList<Node> linkedList) 
	{
		nbEdge++;
		String twoNode=string.split("\\[")[0];
		int sour=Integer.parseInt(twoNode.split("--")[0].trim());
		int targ=Integer.parseInt(twoNode.split("--")[1].trim());
		String attributs=string.split("\\[")[1].split("\\]")[0];
		String defaultAttributes=attributs.split(";,")[0].trim();
		String otherAttributes=attributs.split(";,")[1].trim();

		
//		//----Traitement propriété defaut------------------------------------;
		String label=defaultAttributes.split("label=")[1].split(",")[0];
		String thickness_=defaultAttributes.split("thickness=")[1].split(",")[0];
		String color_=defaultAttributes.split("color=")[1];
		this.name=label;
		this.thickness=Integer.parseInt(thickness_);
		int red=Integer.parseInt(color_.split(";")[0]);
		int green=Integer.parseInt(color_.split(";")[1]);
		int blue=Integer.parseInt(color_.split(";")[2]);
		this.color=new Color(red,green,blue);

//		//----Traitement propriété autre-----------------------3");
		for (String s: otherAttributes.split(",")) 
		{
			String key=s.trim().split("=")[0].trim();
			String value=s.trim().split("=")[1].trim();
			int val=Integer.parseInt(value);
			this.addAttributes(key, val);
		}
		for (Node node : linkedList) {
			if(node.getIdNode()==sour)
			{
				source=node;
			}
			if(node.getIdNode()==targ)
			{
				target=node;
			}
		}
	}
	
	/**
     * Retourne la couleur de l'ar�te
     * @return Couleur  de l'ar�te
     */
	public Color getColor() {
		return color;
	}
	
	/**
     * Affecte une nouvelle couleur � l'arete
     * @param couleur
     * 			La nouvelle couleur
     */
	public void setColor(Color couleur) {
		this.color = couleur;
	}
	
	/**
     * Retourne l'�paisseur de l'ar�te
     * @return L'�paisseur de l'ar�te 
     */
	public int getThickness() {
		return thickness;
	}
	
	/**
     * Affecte une nouvelle epaisseur a l'arete
     * @param epaisseur
     * 			La nouvelle epaisseur
     */
	public void setThickness(int epaisseur) {
		this.thickness = epaisseur;
	}

	/**
     * Retourne le nom de l'arete
     * @return le nom de l'arete
     */
	public String getName() {
		return this.name;
	}

	/**
     * Affecte un nouveau nom à l'arête
     * @param name
     * 			Le nouveau nom
     */
	public void setName(String name) {
		this.name = name;
	}

	
	/**
    * Retourne le sommet source de l'arete
    * @return  Le sommet source de l'arete
    */
	public Node getSource() {
		return source;
	}


	/**
	    * Retourne le sommet destination de l'arete
	    * @return  Le sommet destination de l'arete
	    */
	public Node getTarget() {
		return target;
	}

	
	
	/**
     * Retourne si l'ar�te touche le rectangle pass� en
     * param�tre
     * @return Si l'ar�te touche le rectangle pass� en
     * param�tre
     */
	public boolean inRectangle(int xRect,int yRect,int wRect,int hRect) 
	{
		Polygon r=new Polygon();
		r.addPoint(source.centerNode().x,source.centerNode().y);
		r.addPoint(target.centerNode().x,target.centerNode().y);
		
		return r.intersects(xRect, yRect, wRect, hRect);
		
		
	}

	/**
     * Défini si l'arrete est s�lectionn�e ou non
     * @param selected
     * 			La selection ou non de l'arete
     */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}


	/**
     * Retourn si l'arete est selectionnée ou non.
     * @return Si l'arete est selectionnée ou non.
     */
	public boolean isSelected() {
		return selected;
	}
	
	/**
	 * Exporte l'arete au format graphml
	 */
	public Element exportEdgeXml(Document doc)
	{
		Element edge=doc.createElement("edge");
		edge.setAttribute("id", idEdge+"");
		edge.setAttribute("source", source.getIdNode()+"");
		edge.setAttribute("target", target.getIdNode()+"");
		
		Element Color=doc.createElement("data");
		Color.setAttribute("key", "e0");
		Color.setTextContent(color.getRed()+";"+color.getGreen()+";"+color.getBlue());
		edge.appendChild(Color);
		
		Element Name=doc.createElement("data");
		Name.setAttribute("key", "e1");
		Name.setTextContent(name);
		edge.appendChild(Name);
		
		Element Thickness=doc.createElement("data");
		Thickness.setAttribute("key", "e2");
		Thickness.setTextContent(thickness+"");
		edge.appendChild(Thickness);
		
		return edge;
	
	}
	public static void addAttributes(Document doc,Element root)
	{
		Element keyColor=doc.createElement("key");
		keyColor.setAttribute("id", "e0");
		keyColor.setAttribute("for", "edge");
		keyColor.setAttribute("attr.name", "color");
		keyColor.setAttribute("attr.type", "Color");
		root.appendChild(keyColor);
		
		Element keyName=doc.createElement("key");
		keyName.setAttribute("id", "e1");
		keyName.setAttribute("for", "edge");
		keyName.setAttribute("attr.name", "name");
		keyName.setAttribute("attr.type", "String");
		root.appendChild(keyName);
		
		Element keyThickness=doc.createElement("key");
		keyThickness.setAttribute("id", "e2");
		keyThickness.setAttribute("for", "edge");
		keyThickness.setAttribute("attr.name", "thickness");
		keyThickness.setAttribute("attr.type", "int");
		root.appendChild(keyThickness);
	}

	/**
     * Retourne si l'ar�te est touchée par le point passé en parametre
     * param�tre
     * @return  si L'ar�te est touchée par le point passé en parametre
     * param�tre
     */
	public boolean containsPoint(Point pt) 
	{
		Polygon p=new Polygon();
		p.addPoint(source.centerNode().x-thickness,source.centerNode().y-thickness);
		p.addPoint(target.centerNode().x-thickness,target.centerNode().y-thickness);
		p.addPoint(source.centerNode().x+thickness,source.centerNode().y+thickness);
		p.addPoint(target.centerNode().x+thickness,target.centerNode().y+thickness);
		
		if(p.intersects(pt.x-2,pt.y-2,2,2))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public String exportEdgeDot()
	{
		String edgeDot=""+source.getIdNode()+"--"+target.getIdNode()+ " ";
		edgeDot+="[";
		
		
		edgeDot+="label="+name+", ";
		edgeDot+="thickness="+getThickness()+", ";
		edgeDot+="color="+color.getRed()+";"+color.getGreen()+";"+color.getBlue()+";,";
		for(Entry<String, Double> entry : edgeAttributes.entrySet()) 
		{
			edgeDot+=entry.getKey()+"="+entry.getValue()+", ";
		}
		edgeDot+="id="+idEdge+"];";
		return edgeDot;
	}
	public void addAttributes(String key,double Value)
	{
		edgeAttributes.put(key, Value);
	}

	public HashMap<String, Double> getEdgeAttributes() {
		
		return edgeAttributes;
	}

	public void removeProperty(String key) {
		edgeAttributes.remove(key);
	}

	public void addAttributes(String key, Double value) {
		edgeAttributes.put(key, value);
	}
}
