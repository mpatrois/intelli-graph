package Model;

import java.util.LinkedList;

/**
* Tous les models instanciés dans les différentes fenetres
* on une référence sur ces éléments copiés
* 
* @author  Intelli'Graph team
* @version 1.0
* 
*/
public class CopiedElements 
{
	public LinkedList<Node> sommetCopier=new LinkedList<Node>();
	public LinkedList<Edge> areteCopier=new LinkedList<Edge>();
}
