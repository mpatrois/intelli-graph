package Model;

import java.awt.Point;

/**
La classe forme spécifie la figure qui va représenter
le sommet dans lequel elle est contenue.
 * <p>
* Une forme est caractérisé par les informations suivantes :
 * <ul>
 * <li>Une position en abscisse posX.</li>
 * <li>Une position en ordonnée posY.</li>
 * <li>Une taille</li>
 * </ul>
 * </p>
 * <p>
 * Forme est abstraite et possède des fonctions permettant :<br>
 * - de connaitre le type de la forme<br>
 * - de la deplacer<br>
 * - de connaitre son centre<br>
 * - de savoir si elle est contenue dans un rectangle de selection ou contients un point<br>
 * </p>
 * 
 * @see Node
 * @see Rectangle
 * @see Circle
 * @see Triangle
 * 
 * @author Intelli'Graph Team
 * @version 1.0
 */
public abstract class Shape 
{
	/**
     * La taille des côtés (ou rayon) d'une forme
     * Elle n'est pas définitive
     * @see Shape#Forme(int x,int y,int _taille)
     * @see Shape#getSize()
     */
	protected int size;
	
	
	/**
     * La position en abscisse de la forme <br>
     * Elle n'est pas définitive
     * @see Shape#Forme(int x,int y,int _taille)
     * @see Shape#getX()
     * @see Shape#setX(int posX)
     */
	protected int posX;
	

	/**
     * La position en ordonnée de la forme <br>
     * Elle n'est pas définitive
     * @see Shape#Forme(int x,int y,int _taille)
     * @see Shape#getY()
     * @see Shape#setY(int posY)
     */
	protected int posY;
	
	
	
	/**
     * Constructeur Forme.
     * <p>
     * A la construction d'un objet Forme, la taille , la position
     * en abscisse et en ordonnée sonts définies
     * </p>
     * 
     * @param x
     *           La positon en abscisse de la forme.
     * @param y
     *           La positon en ordonnée de la forme.
     * @param _taille
     *            La taille de la forme.
     * 
     * @see Shape#posX
     * @see Shape#posY
     * @see Shape#size
     */
	Shape(int x,int y,int _taille)
	{
		posX=x;
		posY=y;
		size=_taille;
	}
	
	/**
     * Déplace la forme en fonction des x et y passés en paramètre <br>
     * @param x
     * Déplace la posX de la forme en x
     * @param y
     * Déplace la posY de la forme en y	
     */
	public void move(int x,int y)
	{
		posX=x;
		posY=y;
	}
	
	/**
     * Dessine la forme sur la zone d'édition en fonction du zoom
     * et de l'origine de la zone d' édition <br>
     * @param g
     * La zone d'édition
     * @param zoom
     * Le zoom appliqué à la zone d'édition
     * @param origin
     * L' origine de la zone d'édition
     */
	//sabstract void tracerSommet(Graphics g, float zoom, Point origin) ;

	/**
     * Retourne si la forme contient la souris.
     *  @param coordX Coordonnée en abscisse de la souris
     *  @param coordY Coordonnée en ordonnée de la souris
     *  
     *  @return true si la forme contient la souris false sinon
     */
	abstract boolean containMouse(int coordX, int coordY) ;

	/**
     * 	@return Le centre de la forme
     */
	abstract Point getCenterShape();

	/**
     * 	@return Le type de la forme
     */
	abstract String getTypeShape();
	
	/**
     * @return La taille de la forme
     */
	public int getSize() {
		return size;
	}

	/**
     * Affecte une nouvelle taille a la forme
     * @param taille
     * 			La taille de la forme
     */
	void setWidth(int taille) {
		this.size = taille;
	}
	
	/**
     * Retourne la position en ordonnée de la forme
     * @return La position en ordonnée
     */
	public int getX() {
		return posX;
	}
	/**
     * Affecte une nouvelle position en abscisse
     * @param posX
     * 			La nouvelle position en abscisse
     */
	public void setX(int posX) {
		this.posX = posX;
	}

	/**
     * Retourne la position en ordonnée de la forme
     * @return La position en ordonnée
     */
	public int getY() {
		return posY;
	}

	/**
     * Affecte une nouvelle position en ordonnée
     * @param posY
     * 			La nouvelle position en ordonnée
     */
	public void setY(int posY) {
		this.posY = posY;
	}

	
	/**
     * Retourne si la forme est dans un rectangle de selection.
     *  @param x Coordonnée en abscisse du rectangle
     *  @param y Coordonnée en ordonnée du la souris
	 *  @param w Coordonnée en abscisse de la souris
     *  @param h Coordonnée en ordonnée de la souris
     *  
     *  @return true si la forme est dans le rectangle false sinon
     */
	abstract boolean inRectangle(int x,int y,int w,int h) ;

	public void moveWithDxAndDy(int dx, int dy) {
		posX+=dx;
		posY+=dy;
	}

//	public void moveWithDxDy(int dx, int dy) {
//		// TODO Auto-generated method stub
//		
//	}

//	public void moveWithDxAndDy(int dx, int dy) {
//		po
//	}
	
	
	
}
