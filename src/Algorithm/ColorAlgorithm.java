package Algorithm;

import java.awt.Color;

import Model.Edge;
import Model.GraphModel;
import Model.Node;

public class ColorAlgorithm extends Algorithm {
	
	String selectedItem="";
	Color colorMin;
	Color colorMax;
	
	
	public ColorAlgorithm(GraphModel model_,String selectedItem_,Color colorMin_,Color colorMax_) 
	{
		super(model_);
		selectedItem=selectedItem_;
		colorMin=colorMin_;
		colorMax=colorMax_;
		
	}
	public void executeAlgorithm()
	{
		double minValueOfNodes=0;
		double maxValueOfNodes=0;
		
		if(!model.getNodes().isEmpty())
		{
			if(model.getNodes().getFirst().getNodeAttributes().get(selectedItem) != null)
			{
				 minValueOfNodes=model.getNodes().getFirst().getNodeAttributes().get(selectedItem) ;
				maxValueOfNodes=model.getNodes().getFirst().getNodeAttributes().get(selectedItem);
			}
		}
		for (Node n : model.getNodes()) 
		{
			
			if(n.getNodeAttributes().get(selectedItem) != null)
			{
				
				double valueNode=n.getNodeAttributes().get(selectedItem);
				if(valueNode<minValueOfNodes)
				{
					minValueOfNodes=valueNode;
				}
				if(valueNode>maxValueOfNodes)
				{
					maxValueOfNodes=valueNode;
				}
			}
		}
		for (Node n : model.getNodes()) 
		{
			
			if(n.getNodeAttributes().get(selectedItem) != null)
			{
				double valueNode=n.getNodeAttributes().get(selectedItem);
				
				
				double newR=colorMin.getRed()+(valueNode-minValueOfNodes)*((colorMax.getRed()-colorMin.getRed())/(maxValueOfNodes-minValueOfNodes*1.0));
				double newB=colorMin.getBlue()+(valueNode-minValueOfNodes)*((colorMax.getBlue()-colorMin.getBlue())/(maxValueOfNodes-minValueOfNodes*1.0));
				double newG=colorMin.getGreen()+(valueNode-minValueOfNodes)*((colorMax.getGreen()-colorMin.getGreen())/(maxValueOfNodes-minValueOfNodes*1.0));

				n.setColor(new Color((int)newR,(int)newG,(int)newB));
					
			}
		}
		
		double minValueOfEdges=0;
		double maxValueOfEdges=0;
		
		if(!model.getNodes().isEmpty())
		{
			if(model.getNodes().getFirst().getNodeAttributes().get(selectedItem) != null)
			{
				minValueOfEdges=model.getNodes().getFirst().getNodeAttributes().get(selectedItem) ;
				maxValueOfNodes=model.getNodes().getFirst().getNodeAttributes().get(selectedItem);
			}
		}
		for (Edge n : model.getEdges()) 
		{
			
			if(n.getEdgeAttributes().get(selectedItem) != null)
			{
				
				double valueNode=n.getEdgeAttributes().get(selectedItem);
				if(valueNode<minValueOfEdges)
				{
					minValueOfEdges=valueNode;
				}
				if(valueNode>maxValueOfNodes)
				{
					maxValueOfNodes=valueNode;
				}
			}
		}
		for (Edge n : model.getEdges()) 
		{
			
			if(n.getEdgeAttributes().get(selectedItem) != null)
			{
				double valueNode=n.getEdgeAttributes().get(selectedItem);
				
				//Interpollation lineaire
				double newR=colorMin.getRed()+(valueNode-minValueOfEdges)*((colorMax.getRed()-colorMin.getRed())/(maxValueOfEdges-minValueOfEdges*1.0));
				double newB=colorMin.getBlue()+(valueNode-minValueOfEdges)*((colorMax.getBlue()-colorMin.getBlue())/(maxValueOfEdges-minValueOfEdges*1.0));
				double newG=colorMin.getGreen()+(valueNode-minValueOfEdges)*((colorMax.getGreen()-colorMin.getGreen())/(maxValueOfEdges-minValueOfEdges*1.0));

				n.setColor(new Color((int)newR,(int)newG,(int)newB));
					
			}
		}
		
	}

	

}
