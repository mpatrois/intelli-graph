package Algorithm;

import java.util.LinkedList;

import Model.Edge;
import Model.GraphModel;
import Model.Node;

public class AlgorithmDegree extends Algorithm
{

	class NodeWithNeighbours
	{
		Node n;
		int color;
		LinkedList<Node> voisins;
		NodeWithNeighbours(Node n,LinkedList<Edge> edge)
		{
			this.n=n;
		}
	}
	public AlgorithmDegree(GraphModel model_) {
		super(model_);
		// TODO Auto-generated constructor stub
	}

	@Override
	public
	void executeAlgorithm() 
	{
		for (Node n : model.getNodes()) 
		{
			int nbNeighbours=0;
			for (Edge e : model.getEdges()) 
			{
				if(e.getTarget().equals(n))
				{
					nbNeighbours++;
				}
				if(e.getSource().equals(n))
				{
					nbNeighbours++;
				}
			}
			n.addAttributes("Degré sommet", nbNeighbours);
		}
	}

}

