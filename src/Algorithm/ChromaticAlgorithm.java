package Algorithm;

import java.awt.Color;
import java.util.Collections;
import java.util.LinkedList;

import Model.Edge;
import Model.GraphModel;
import Model.Node;

public class ChromaticAlgorithm extends Algorithm {
	
	LinkedList<NodeColor> nodesCol;
	LinkedList<Color> couleur;

	class NodeColor
	{
		Node n;
		int color;
		LinkedList<Node> voisins;
		NodeColor(Node n,LinkedList<Edge> edge)
		{
			this.n=n;
			color=0;
			voisins=new LinkedList<Node>();
			for (Edge e : edge) 
			{
				if(e.getTarget().equals(n))
				{
					voisins.add(e.getSource());
				}
				if(e.getSource().equals(n))
				{
					voisins.add(e.getTarget());
				}
			}
		}
		
		public int petiteCouleurVoisins()
		{
			int min=0;
			for (Node nod : voisins) 
			{
				if(getNodeColor(nod).color>=min)
				{
					min=getNodeColor(nod).color+1;
				}
			}
			color=min;
			return min;
		}
		private NodeColor getNodeColor(Node nod) 
		{
			for (NodeColor nodeColor : nodesCol) {
				if(nodeColor.n.equals(nod))
					return nodeColor;
			}
			return null;
		}
		
	}
	public ChromaticAlgorithm(GraphModel model)
	{
		super(model);
		couleur=new LinkedList<Color>();
		for (int i = 0; i < 255; i+=10) {
			for (int j = 0; j < 255; j+=10) {
				for (int j2 = 0; j2 < 255; j2+=10) {
					couleur.add(new Color(i,j,j2));
				}
			}
		}
		Collections.shuffle(couleur);
		
		nodesCol=new LinkedList<NodeColor>();
		for (Node n :model.getNodes()) 
		{
			NodeColor nColor=new NodeColor(n,model.getEdges());
			this.nodesCol.add(nColor);
		}
	}
	public void executeAlgorithm()
	{
		for (NodeColor n : nodesCol) 
		{
			int col=n.petiteCouleurVoisins();
			
				n.n.setColor(couleur.get(col));
			
		}
	}
}
