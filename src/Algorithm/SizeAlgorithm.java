package Algorithm;

import Model.GraphModel;
import Model.Node;

public class SizeAlgorithm extends Algorithm{

	String selectedItem="";
	int valueMin=0;
	int valueMax=0;
	public SizeAlgorithm(GraphModel model_,String selectedItem_,int valueMin_,int valueMax_) 
	{
		super(model_);
		selectedItem=selectedItem_;
		valueMin=valueMin_;
		valueMax=valueMax_;	
	}
	public void executeAlgorithm()
	{
		double minValueOfNodes=0;
		double maxValueOfNodes=0;
		if(!model.getNodes().isEmpty())
		{
			if(model.getNodes().getFirst().getNodeAttributes().get(selectedItem) != null)
			{
				 minValueOfNodes=model.getNodes().getFirst().getNodeAttributes().get(selectedItem) ;
				maxValueOfNodes=model.getNodes().getFirst().getNodeAttributes().get(selectedItem);
			}
		}
		for (Node n : model.getNodes()) 
		{
			
			if(n.getNodeAttributes().get(selectedItem) != null)
			{
				
				double valueNode=n.getNodeAttributes().get(selectedItem);
				if(valueNode<minValueOfNodes)
				{
					minValueOfNodes=valueNode;
				}
				if(valueNode>maxValueOfNodes)
				{
					maxValueOfNodes=valueNode;
				}
			}
		}
		for (Node n : model.getNodes()) 
		{
			
			if(n.getNodeAttributes().get(selectedItem) != null)
			{
				double valueNode=n.getNodeAttributes().get(selectedItem);
				//Interpollation lineaire
				double newValueSize=valueMin+(valueNode-minValueOfNodes)*((valueMax-valueMin)/(maxValueOfNodes-minValueOfNodes)*1.0);
				if(newValueSize<=0)
				{
					newValueSize=valueMin;
				}
				n.setSize((int) newValueSize);
					
			}
		}
	}

}
