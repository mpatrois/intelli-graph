package Algorithm;

import java.util.LinkedList;

import Model.Edge;
import Model.GraphModel;
import Model.Node;

public class DegreeAlgorithm extends Algorithm
{

	class NodeWithNeighbours
	{
		Node n;
		int color;
		LinkedList<Node> voisins;
	}
	public DegreeAlgorithm(GraphModel model_) {
		super(model_);
	}

	@Override
	public
	void executeAlgorithm() 
	{
		for (Node n : model.getNodes()) 
		{
			int nbNeighbours=0;
			for (Edge e : model.getEdges()) 
			{
				if(e.getTarget().equals(n))
				{
					nbNeighbours++;
				}
				if(e.getSource().equals(n))
				{
					nbNeighbours++;
				}
			}
			n.addAttributes("Degré sommet", nbNeighbours);
		}
	}

}

