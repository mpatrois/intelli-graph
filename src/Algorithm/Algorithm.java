package Algorithm;

import Model.GraphModel;

public abstract class Algorithm 
{
	GraphModel model=null;
	Algorithm(GraphModel model_)
	{
		model=model_;
	}
	abstract void executeAlgorithm();
}
