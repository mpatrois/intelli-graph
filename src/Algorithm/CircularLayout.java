package Algorithm;
import java.awt.Dimension;
import Model.GraphModel;
import Model.Node;

public class CircularLayout extends Algorithm  {


  private boolean isCircle;

  /**
   * Creates a new CircleLayout that lays out components in a perfect circle
   */

  public CircularLayout(GraphModel model) 
  {
	  super(model);
	  isCircle=true;
  }

  public void executeAlgorithm() {
	    int x, y, w, s, c;
	    int n = model.getNodes().size();

	    int centerX = 0;
	    int centerY = 0;

	    Node comp = null;
	    Dimension compPS = null;
	    if (n == 1) 
	    {
	      comp = model.getNodes().get(0);
	      x = centerX;
	      y = centerY;
	      compPS = new Dimension(model.getNodes().get(0).getSize(),model.getNodes().get(0).getSize());
	      w = compPS.width;
	      comp.setBounds(x, y, w);
	    } else 
	    {
	      double r = 400;
	      r *= 0.75; // Multiply by .75 to account for extreme right and bottom
	                  // Components
	      for (int i = 0; i < n; i++) 
	      {
	        comp = model.getNodes().get(i);
	        compPS = new Dimension(model.getNodes().get(i).getSize(),model.getNodes().get(i).getSize());
	        if (isCircle) {
	          c = (int) (r * Math.cos(2 * i * Math.PI / n));
	          s = (int) (r * Math.sin(2 * i * Math.PI / n));
	        } else 
	        {
	          c = (int) ((centerX * 0.75) * Math.cos(2 * i * Math.PI / n));
	          s = (int) ((centerY * 0.75) * Math.sin(2 * i * Math.PI / n));
	        }
	        x = c + centerX;
	        y = s + centerY;

	        w = compPS.width;
	        comp.setBounds(x, y, w);
	      }
	    }

	  }

}