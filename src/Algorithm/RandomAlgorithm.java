package Algorithm;

import java.awt.Point;
import java.util.Random;
import Model.GraphModel;
import Model.Node;

public class RandomAlgorithm extends Algorithm
{
	int minX;
	int minY;
	int maxX;
	int maxY;
	
	public RandomAlgorithm(GraphModel model_, Point[] points) {
		super(model_);
		minX=points[0].x;
		minY=points[0].y;
		maxX=points[1].x;
		maxY=points[1].y;
	}

	@Override
	public
	void executeAlgorithm() 
	{
		for (Node s : model.getNodes()) 
		{
			Random r = new Random();
			int rx = r.nextInt(maxX-minX) + minX;
			int ry = r.nextInt(maxY-minY) + minY;
			s.move(rx, ry);
			
		};
	}

}

